import pandas as pd
import os
import numpy as np
import pandas as pd
import datetime
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.patches as mpatch
import matplotlib.dates as mdates
from scipy import stats
import matplotlib.dates as mdates
from matplotlib.ticker import MaxNLocator
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error, r2_score
import matplotlib.patches as mpatches
import pyodbc
import sys
from bisect import bisect
from matplotlib.text import Annotation
from matplotlib.transforms import Affine2D
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email import encoders

server = 'cleantechsolar.database.windows.net'
database = 'Cleantech Meter Readings'
username = 'RohanKN'
password = 'R@h@nKN1'
driver= '{ODBC Driver 17 for SQL Server}'

class LineAnnotation(Annotation):
    def __init__(
        self, text, line, x , line_type, xytext=(0, 5), textcoords="offset points", **kwargs
    ):
        assert textcoords.startswith(
            "offset "
        ), "*textcoords* must be 'offset points' or 'offset pixels'"

        self.line = line
        self.xytext = xytext

        # Determine points of line immediately to the left and right of x
        xs, ys = line.get_data()

        assert (
            np.diff(xs) >= 0
        ).all(), "*line* must be a graph with datapoints in increasing x order"

        i = np.clip(bisect(xs, x), 1, len(xs) + 1)
        self.neighbours = n1, n2 = np.asarray([(xs[i - 1], ys[i - 1]), (xs[i], ys[i])])

        # Calculate y by interpolating neighbouring points
        if(line_type == 'below'):
            y = n1[1] + ((x - n1[0]) * (n2[1] - n1[1]) / (n2[0] - n1[0]))-3
        else:
            y = n1[1] + ((x - n1[0]) * (n2[1] - n1[1]) / (n2[0] - n1[0]))

        kwargs = {
            "horizontalalignment": "center",
            "rotation_mode": "anchor",
            **kwargs,
        }
        super().__init__(text, (x, y), xytext=xytext, textcoords=textcoords, **kwargs)

    def get_rotation(self):

        transData = self.line.get_transform()
        dx, dy = np.diff(transData.transform(self.neighbours), axis=0).squeeze()
        return np.rad2deg(np.arctan2(dy, dx))

    def update_positions(self, renderer):
        xytext = Affine2D().rotate_deg(self.get_rotation()).transform(self.xytext)
        self.set_position(xytext)
        super().update_positions(renderer)


def line_annotate(text, line, x , line_type, *args, **kwargs):
    ax = line.axes
    a = LineAnnotation(text, line, x, line_type, *args, **kwargs)
    if "clip_on" in kwargs:
        a.set_clip_path(ax.patch)
    ax.add_artist(a)
    return a

def get_total_eac(date):
    connStr = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
    SQL_Query = pd.read_sql_query("SELECT SUM([Eac-MFM]) AS SUM FROM [dbo].[Stations_Data] WHERE Station_Id=(SELECT Station_Id FROM [dbo].[Stations] WHERE Station_Name='"+site+"') AND Date='"+date+"' ", connStr)
    df_sum = pd.DataFrame(SQL_Query, columns=['SUM'])
    return df_sum
    
#Sending Message
def send_mail(date, stn, df, recipients, attachment_path_list=None):
  info ='Date: '+date+'\n\n'
  info = info+"GHI : "+str(df[0]['GHI'].values[0])+'\n\n'
  info = info+"PR : "+str(round(df[0]['PR'].values[0],1))+'\n\n'
  server = smtplib.SMTP("smtp.office365.com")
  server.starttls()
  server.login('shravan.karthik@cleantechsolar.com', 'CTS&*(789')
  msg = MIMEMultipart()
  sender = 'operations@cleantechsolar.com'
  msg['Subject'] = stn 
  today_g = df_today['GHI'].values[0]
  today_pr = df_today['PR'].values[0]
  if('Upper' in stn):
      info = info+'Alarm type: Upper Corridor\n\n'
      trig_val = round((((today_pr-(slope*today_g+(intercept)))/today_pr))*100,1)
  elif('Lower' in stn):
      info = info+'Alarm type: Lower Corridor\n\n'
      temp_trig = (slope*today_g+(intercept))
      trig_val = round(((temp_trig - today_pr)/temp_trig)*100,1)
  #if sender is not None:
  msg['From'] = sender
  msg['To'] = ", ".join(recipients)
  info = info + 'Alarm Trigger Value: '+str(trig_val)+' %\n\n'
  if attachment_path_list is not None:
    for each_file_path in attachment_path_list:
      try:
        file_name = each_file_path.split("/")[-1]
        part = MIMEBase('application', "octet-stream")
        part.set_payload(open(each_file_path, "rb").read())
        Encoders.encode_base64(part)
        part.add_header('Content-Disposition', 'attachment' ,filename=file_name)
        msg.attach(part)
      except:
        print("could not attache file")
  msg.attach(MIMEText(info,'plain/text'))
  server.sendmail(sender, recipients, msg.as_string())
  
def abline(slope, intercept, df, df_today):
    x_vals = np.array(ax.get_xlim())
    y_vals = intercept + slope * x_vals
    y_vals2 = (intercept+0.05*intercept) + slope * x_vals
    y_vals3 = (intercept-0.05*intercept) + slope * x_vals
    g = df['GHI'].tolist()
    p = df['PR'].tolist()
    cnt=0
    for index,i in enumerate(g):
        temp = slope*i+(intercept+0.05*intercept)
        temp2 = slope*i+(intercept-0.05*intercept)
        if(p[index]>temp or p[index]<temp2):
            cnt=cnt+1
    total = len(df)
    ax.annotate('Inside corridor = '+str(total-cnt)+'/'+str(total)+' = '+str(round(((total-cnt)/total)*100,1))+'%', (.99, .05),xytext=(4, -4),xycoords='axes fraction',textcoords='offset points',fontweight='bold',color='black',ha='right', va='bottom',size=11)
    plt.plot(x_vals, y_vals,color='darkgreen')
    line_1 = plt.plot(x_vals, y_vals2,'--',color='#008000')
    line_2 = plt.plot(x_vals, y_vals3,'--',color='#008000')
    line_annotate('+5% variation from mean',line_1[0],4,'above',color='#008000')
    line_annotate('-5% variation from mean',line_2[0],3.8,'below',color='#008000')
"""     if((today_pr>slope*today_g+(intercept+0.05*intercept))):
        send_mail(df_today['Date'].values[0],'Upper '+site+' PR Corridor Alarm',[df_today,slope,intercept],['operationsSG@cleantechsolar.com','sai.pranav@cleantechsolar.com'])
    elif((today_pr<slope*today_g+(intercept-0.05*intercept))):
        send_mail(df_today['Date'].values[0],'Lower '+site+' PR Corridor Alarm',[df_today,slope,intercept],['operationsSG@cleantechsolar.com','sai.pranav@cleantechsolar.com'])
 """

    
font = {'size'   : 11}

matplotlib.rc('font', **font)


""" path = '/home/admin/Dropbox/Cleantechsolar/1min/[728]/2020/2020-12/'
df_final = pd.DataFrame()
for i in sorted(os.listdir(path)):
    print(i)
    df = pd.read_csv(path+i,sep='\t')
    df_final = df_final.append(df)
    df_final = df_final[['Tm','Act_Pwr-Tot_Pond','AvgGsi00_Shr']]
print(df_final)
df_final.to_csv('Extract.txt',sep='\t') """
#date_today = sys.argv[2]

mfm_path = 'Extract.txt'
df_mfm = pd.read_csv(mfm_path,sep='\t')
df_mfm = df_mfm[['Tm','Act_Pwr-Tot_Pond','AvgGsi00_Shr']]
df_mfm = df_mfm[df_mfm['AvgGsi00_Shr']>0]
df_mfm = df_mfm[df_mfm['Act_Pwr-Tot_Pond']>0]
df = df_mfm
df['Tm'] = pd.to_datetime(df['Tm'])
df.index = df['Tm']
df = df.resample('1h').sum()
df = df[df.index.hour>=6]
df = df[df.index.hour<=17]
df['Eac'] = df['Act_Pwr-Tot_Pond']/60
df['GHI'] = df['AvgGsi00_Shr']/60
df['PR'] = ((df['Eac']/2.83532)/df['GHI'])*100
print(df)
shape_dict = {6:'h',7:'v',8:'>',9:'s',10:'^',11:'o',12:'d',13:'1',14:'2',15:'<',16:'4',17:'p'}

fig3 = plt.figure(num=None, figsize=(13.2  , 8.8))

ax = fig3.add_subplot(111)

fig3 = plt.figure(num=None, figsize=(13.2  , 8.8))

legends = []
vals = []
dic = {}
ax = fig3.add_subplot(111)
for index,row in df.iterrows():
    if(row['GHI']<300):
        c = 'darkblue'
    elif(row['GHI']>300 and row['GHI']<=600):
        c = 'deepskyblue'
    elif(row['GHI']>600 ):
        c = 'orange'
    ln = ax.scatter(row['GHI'], row['PR'], marker=shape_dict[index.hour],color=c,s=60, alpha=0.75, zorder=3)
    dic[index.hour] = ln

plt.ylim(0,110)
ax.set_ylabel("PR [%]")
ax.set_xlabel("GHI [Wh/m2]")

for i in dic:
    vals.append(i)
    legends.append(dic[i])
for index,i in enumerate(vals):
    vals[index]=str(i)+':00'
    
g_patch = mpatches.Patch(color='white', label='GHI [W/m2]')
kh_patch = mpatches.Patch(color='darkblue', label='< 300')
th_patch = mpatches.Patch(color='deepskyblue', label='300 ~ 600')
vn_patch = mpatches.Patch(color='orange', label='>600')




legend = plt.legend(legends,vals,scatterpoints=1,fontsize=11,loc=(0.85,.05),frameon=False,title='Time')

totlen=len(vals)
for m in range(totlen):
    legend.legendHandles[m].set_color('black')
    
plt.gca().add_artist(legend)
plt.legend(handles=[g_patch,kh_patch,th_patch,vn_patch],loc=(0.41,.92),frameon=False,ncol=5,fontsize=11)

ttl_main=fig3.suptitle('KH-008 GHI vs PR',fontsize=15,x=.512)
fig3.savefig("/home/admin/Graphs/Graph_Output/"+'KH-008'+"/["+'KH-008'+"] Graph - GHI vs PR All.png", bbox_inches='tight')
 