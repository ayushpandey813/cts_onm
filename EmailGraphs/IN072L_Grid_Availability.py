import requests, json	
import requests.auth	
import pandas as pd	
import datetime	
import os	
import re	
import time	
import shutil	
import pytz	
import sys	
import pyodbc	
import numpy as np	
import matplotlib	
matplotlib.use('Agg')	
import matplotlib.pyplot as plt	
import matplotlib.patches as mpatch	
from matplotlib.dates import DateFormatter	
import matplotlib.dates as mdates	
from datetime import date	
from datetime import timedelta
from datetime import datetime

enddate=sys.argv[1]

MFM_path_read='/home/admin/Dropbox/Fourth_Gen/[IN-072L]/[IN-072L]-lifetime.txt'
WMS_path_read='/home/admin/Dropbox/Fourth_Gen/[IN-061L]/[IN-061L]-lifetime.txt'
extract_path='/home/admin/Graphs/Graph_Extract/IN-072/[IN-072] Graph ' + enddate + ' - Grid Availability.txt'
graph_path='/home/admin/Graphs/Graph_Output/IN-072/[IN-072] Graph ' + enddate + ' - Grid Availability.pdf'

#Extracting Data
df1=pd.read_csv(MFM_path_read, sep='\t')
df1=df1[['Date', 'MFM_1.GA']]
df2=pd.read_csv(WMS_path_read, sep='\t')
df2=df2[['Date', 'WMS_1.DA']]
df=pd.merge(df2, df1, how='inner', on='Date', sort=True)
df.columns=['Date', 'WMS_DA', 'GA']
df.Date=pd.to_datetime(df.Date)

#Calculating GA for Rogue Days
for i in range(len(df)):
  if df.WMS_DA[i] < 50:
    temp_date=str(df.Date[i].date())
    temp_read='/home/admin/Dropbox/Gen 1 Data/[IN-072L]/' + temp_date[:4] + '/' + temp_date[:7] + '/MFM_1/[IN-072L]-MFM1-' + str(df.Date[i].date()) + '.txt'
    if(os.path.exists(temp_read)):
      temp=pd.read_csv(temp_read, sep='\t')
      temp=temp[(temp.ts >= temp_date + ' 09:00:00') & (temp.ts <= temp_date + ' 18:00:00')]
      FrequencyGreater=temp[temp.Hz_avg>40]['ts']
      common=pd.merge(temp, FrequencyGreater, how='inner')
      if len(temp)>0:
        df.GA[i]=round((len(common)*100/len(temp)), 1)
      else:
        df.GA[i]=0
    else:
        df.GA[i]=0

#Generating Graph
df=df[df.GA>0]
df=df[df.Date <=enddate]
df=df.tail(60)
df.reset_index(drop=True, inplace=True)
startdate=str(df['Date'][0].date())
dates=list(df.Date)
GAs=list(df.GA)
plt.rcParams.update({'font.size': 12})
plt.rcParams['axes.facecolor']='gainsboro'
fig=plt.figure(num=None, figsize=(13, 8.5))
ax=fig.add_subplot(111)
plt.plot(dates, GAs, marker='o', MarkerSize='2', color='red', linewidth=1.5)
plt.xticks(dates)
date_form=DateFormatter("%d-%b")
ax.xaxis.set_major_formatter(date_form)
ax.xaxis.set_major_locator(mdates.WeekdayLocator(byweekday=1, interval=1))
plt.ylim([0, 105])
ttl=ax.set_title('From ' + startdate + ' to ' + enddate, fontdict={'fontsize': 11, 'fontweight': 'bold'})
ttl.set_position([.485, 1.04])
ttl_main=fig.suptitle('Grid Availability Evolution - IN-072', fontsize=11, fontweight='bold')
lifetime_GA='Lifetime GA: ' + str(round(sum(GAs)/len(GAs), 2))
no_of_points='No. of Points: ' + str(len(GAs)) + ' days'
plt.text(0.87, 0.15, no_of_points, fontsize=9, transform=plt.gcf().transFigure, weight='bold', color='white', ha='right', bbox=dict(facecolor='black', alpha=0.8, pad=8))
plt.text(0.87, 0.20, lifetime_GA, fontsize=9, transform=plt.gcf().transFigure, weight='bold', color='white', ha='right', bbox=dict(facecolor='black', alpha=0.8, pad=8))

for i in range(len(GAs)):
  if GAs[i] < 70:
    ln1=plt.scatter(dates[i], GAs[i], marker='D', color='green', s=20)
    ax.annotate(dates[i].date(), (dates[i], GAs[i] + 1.25), size=9, color='green')

ax.set_ylabel('Grid Availability [%]', fontsize=12)

df.to_csv(extract_path, sep='\t', index=False, header=True)
fig.savefig(graph_path, bbox_inches='tight', pad_inches=0.1)