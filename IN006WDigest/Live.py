from ftplib import FTP
import pandas as pd
import time
import datetime
import pytz
import io
import urllib
import re 
import os
import numpy as np
import gzip
import shutil
import logging
import sqlalchemy as sa
import urllib.request


def chkdir(path):
    a=os.path.isdir(path)
    if(a):
        return
    else:
        os.makedirs(path)

def write(Type,cols,path_temp,df,date):
    df.columns=cols
    date=str(date)
    chkdir(path_temp+'/'+Type)
    fileType=Type
    if(Type[0:3]=='INV'):
        fileType='I'+Type.split('_')[1]
    elif(Type[0:3]=='MFM'):
        fileType='MFM'+Type.split('_')[1]
    elif(Type[0:3]=='WMS'):
        fileType='WMS'+Type.split('_')[1]
    df['Date']=pd.to_datetime(df['Date'])
    if(os.path.exists(path_temp+'/'+Type+'/'+'[IN-006W]-'+fileType+'-'+date[0:10]+'.txt')):
        df.to_csv(path_temp+'/'+Type+'/'+'[IN-006W]-'+fileType+'-'+date[0:10]+'.txt',sep='\t',index=False,header=False,mode='a')
    else:
        df.to_csv(path_temp+'/'+Type+'/'+'[IN-006W]-'+fileType+'-'+date[0:10]+'.txt',sep='\t',index=False,header=True,mode='a')

def filter_files(names,file_name):
    return [k for k in names if file_name in k]



path='/home/admin/Dropbox/Gen 1 Data/[IN-006W]/'
startpath="/home/admin/Start/IN006W.txt"
#logging.basicConfig(filename='/home/admin/Logs/LogsIN013History.txt')

chkdir(path)
tz = pytz.timezone('Asia/Kolkata')
curr=datetime.datetime.now(tz)

print("Start time is",curr)
print("Starting Live Bot!")


mfmcols=['Date','Apparent power','Active power','Current average','Apparent power, phase 1','Active power, phase 1','Current, phase 1','Apparent power, phase 2','Active power, phase 2','Current, phase 2','Apparent power, phase 3','Active power, phase 3','Current, phase 3','Forward apparent energy','Forward active energy','n hours']
wmscols=['Date','Irradiance_Min','Irradiance_Max','Irradiance_Avg','Temp_1','Temp_2','Temp_3','Temp_4','Temp_5','Temp_6','Temp_7','Temp_8','Temp_9','Temp_10','Temp_11','Temp_12','Temp_13','Temp_14','Temp_15','Temp_16','Temp_17']
invcols=['Date','Total Energy Production_min', 'Total Energy Production_max', 'Total Energy Production_avg', 'PV Voltage input 1_min', 'PV Voltage input 1_max', 'PV Voltage input 1_avg', 'PV Voltage input 2_min', 'PV Voltage input 2_max', 'PV Voltage input 2_avg', 'PV Voltage input 3_min', 'PV Voltage input 3_max', 'PV Voltage input 3_avg', 'PV Current input 1_min', 'PV Current input 1_max', 'PV Current input 1_avg', 'PV Current input 2_min', 'PV Current input 2_max', 'PV Current input 2_avg', 'PV Current input 3_min', 'PV Current input 3_max', 'PV Current input 3_avg', 'Instant Energy Production_min', 'Instant Energy Production_max', 'Instant Energy Production_avg', 'Grid Voltage_min', 'Grid Voltage_max', 'Grid Voltage_avg', 'Grid Current_min', 'Grid Current_max', 'Grid Current_avg', 'Grid Frequency_min', 'Grid Frequency_max', 'Grid Frequency_avg']

components={'WD00D47C':{1:['MFM_1_ABS',mfmcols,16]},
'WD00DE9F':{1:['MFM_2_EMR',mfmcols,16]},
'WD00D486':{1:['MFM_3_ER&D',mfmcols,16]},
'WD00D4BC':{1:['MFM_4_SS',mfmcols,16]},
'WD00E05E':{1:['MFM_5_Unit21',mfmcols,16]},
'WD00D477':{1:['MFM_6_Unit24',mfmcols,16]},
'WD00D47C_INV':{1:['INVERTER_1_ABS',invcols,34],2:['INVERTER_2_ABS',invcols,34],3:['INVERTER_3_ABS',invcols,34],4:['INVERTER_4_ABS',invcols,34],5:['INVERTER_5_ABS',invcols,34]},
'WD00DE9F_INV':{1:['INVERTER_1_EMR',invcols,34],2:['INVERTER_2_EMR',invcols,34],3:['INVERTER_3_EMR',invcols,34]},
'WD00D486_INV':{1:['INVERTER_1_ER&D',invcols,34],2:['INVERTER_2_ER&D',invcols,34]},
'WD00D4BC_INV':{1:['INVERTER_1_SS',invcols,34],2:['INVERTER_2_SS',invcols,34],3:['INVERTER_3_SS',invcols,34],4:['INVERTER_4_SS',invcols,34],5:['INVERTER_5_SS',invcols,34]},
'WD00E05E_INV':{1:['INVERTER_1_Unit21',invcols,34],2:['INVERTER_2_Unit21',invcols,34],3:['INVERTER_3_Unit21',invcols,34],4:['INVERTER_4_Unit21',invcols,34],5:['INVERTER_5_Unit21',invcols,34],6:['INVERTER_6_Unit21',invcols,34],7:['INVERTER_7_Unit21',invcols,34],8:['INVERTER_8_Unit21',invcols,34],9:['INVERTER_9_Unit21',invcols,34],10:['INVERTER_10_Unit21',invcols,34]},
'WD00D477_INV':{1:['INVERTER_1_Unit24',invcols,34],2:['INVERTER_2_Unit24',invcols,34]},
}

def chk_date(date):
    try:
        date=datetime.datetime.strptime(date[0:6]+'20'+date[6:], '%d/%m/%Y-%H:%M:%S')
        return 1
    except:
        return 0


if(os.path.exists(startpath)):
    pass
else:
    try:
        shutil.rmtree(path,ignore_errors=True)
    except Exception as e:
        print(e)
    with open(startpath, "w") as file:
        file.write("2021-01-27\n00:00:00")   
with open(startpath) as f:
    startdate = f.readline(11)[:-1]
    starttime = f.readline(10)
print(startdate)
print(starttime)

for c in components:
    print(c)
    if(len(c.split('_'))>1):
        ftp = FTP('ftpnew.cleantechsolar.com', timeout=120)
        ftp.login(user='IN-006W', passwd = 'P9VBCL38Ma4mECFm')
        ftp.cwd('DATA/INV')
        files=ftp.nlst() 
        print('Doing WMS')
    else:
        ftp = FTP('ftpnew.cleantechsolar.com', timeout=120)
        ftp.login(user='IN-006W', passwd = 'P9VBCL38Ma4mECFm')
        ftp.cwd('DATA/MODBUS')
        files=ftp.nlst() 
    start=startdate+starttime
    start=start.replace('\n','')
    start=datetime.datetime.strptime(start, "%Y-%m-%d%H:%M:%S")
    end=datetime.datetime.now(tz).replace(tzinfo=None)-datetime.timedelta(minutes=1)
    while(start<end):
        try:
            files_temp=filter_files(files,c)
            start_str=start.strftime("%Y%m%d_%H%M")
            start_str='_'+start_str[2:]
 
            for i in sorted(files_temp):
                if start_str in i:
         
                    path_temp=path+'20'+start_str[1:3]+'/'+'20'+start_str[1:3]+'-'+start_str[3:5]
                    chkdir(path_temp)
                    if(len(c.split('_'))>1):
                        print('doing inv')
                        req = urllib.request.Request('ftp://IN-006W:P9VBCL38Ma4mECFm@ftpnew.cleantechsolar.com/DATA/INV/'+i)
                    else:
                        req = urllib.request.Request('ftp://IN-006W:P9VBCL38Ma4mECFm@ftpnew.cleantechsolar.com/DATA/MODBUS/'+i)
                    with urllib.request.urlopen(req) as response:
                        s =  gzip.decompress(response.read())
                    cols = [1, 2, 3, 4, 5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60]
                    df=pd.read_csv(io.StringIO(s.decode('utf-8')),sep=';',names=cols)
                    for index, row in df.iterrows():
                        if(row[1]=='ADDRMODBUS' or row[1] == 'SNINV'):
                            val=row[3]
                        if(chk_date(row[1])):
                            data=row.to_frame().T.iloc[:,:components[c][int(val)][2]]
                            date=datetime.datetime.strptime(row[1][0:6]+'20'+row[1][6:], '%d/%m/%Y-%H:%M:%S')
                            if(date.year<2015):
                                continue
                            data[1]=date.replace(second=0)
                            if(len(c.split('_'))>1):#WMS Factor Multiplication
                                data[4]=(1.456*float(data[4].values[0]))-291.2
                            write(components[c][int(val)][0],components[c][int(val)][1],path_temp,data,date)
            start=start+datetime.timedelta(minutes=1)
        except Exception as e:
            print(e)
            start=start+datetime.timedelta(minutes=1)
            logging.exception("His")
            pass
ftp.close()
print('Historical Done!')
flag=[0,0,0,0,0,0,0,0,0,0,0,0]
files_sorted=[]
while(1):
    try:
        time_now=datetime.datetime.now(tz)
        print('Live',time_now)
        ftp = FTP('ftpnew.cleantechsolar.com', timeout=120)
        ftp.login(user='IN-006W', passwd = 'P9VBCL38Ma4mECFm')
        print('Connected!')
        cnt=0
        new_sorted=[]
        for c in sorted(components):#sort to preserve order
            if(len(c.split('_'))>1):
                ftp = FTP('ftpnew.cleantechsolar.com', timeout=120)
                ftp.login(user='IN-006W', passwd = 'P9VBCL38Ma4mECFm')
                print('Connected!')
                ftp.cwd('DATA/INV')
                files=ftp.nlst() 
            else:
                ftp = FTP('ftpnew.cleantechsolar.com', timeout=120)
                ftp.login(user='IN-006W', passwd = 'P9VBCL38Ma4mECFm')
                print('Connected!')
                ftp.cwd('DATA/MODBUS')
                files=ftp.nlst() 
            if(flag[cnt]==0):
                start=time_now
                files_temp=filter_files(files,c)
                files_sorted.append(sorted(files_temp,reverse=True))
                new_file=[files_sorted[cnt][0]]
                flag[cnt]=1
                print(c)
                time.sleep(1)
            else:
                files_temp=filter_files(files,c)  
                new_sorted.append(sorted(files_temp,reverse=True))
                with open("/home/admin/Start/MasterMail/IN-006W_FTPProbe.txt", "w") as file:
                    file.write(str(time_now.replace(microsecond=0)))
                asd=new_sorted[cnt]
                new_file=list(set(new_sorted[cnt]) - set(files_sorted[cnt]))
                files_sorted[cnt]=new_sorted[cnt]
            for i in sorted(new_file):
                print(flag)
                print(new_file)
                time.sleep(2)
                if(len(c.split('_'))>1):
                    date=i.split('_')[4]
                else:
                    date=i.split('_')[2]
                path_temp=path+'20'+date[0:2]+'/'+'20'+date[0:2]+'-'+date[2:4]
                chkdir(path_temp)
                if(len(c.split('_'))>1):
                    url='ftp://IN-006W:P9VBCL38Ma4mECFm@ftpnew.cleantechsolar.com/DATA/INV/'+i
                else:
                    url='ftp://IN-006W:P9VBCL38Ma4mECFm@ftpnew.cleantechsolar.com/DATA/MODBUS/'+i
                try:
                    response = urllib.request.urlopen(url, timeout=120).read()
                except (HTTPError, URLError) as error:
                    logging.error('Data of %s not retrieved because %s\nURL: %s', name, error, url)
                except timeout:
                    logging.error('socket timed out - URL %s', url)
                else:
                    logging.info('Access successful.')
                s =  gzip.decompress(response)
                cols = [1, 2, 3, 4, 5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60]
                df=pd.read_csv(io.StringIO(s.decode('utf-8')),sep=';',names=cols)
                for index, row in df.iterrows():
                    if(row[1]=='ADDRMODBUS' or row[1] == 'SNINV'):
                        val=row[3]
                    if(chk_date(row[1])):
                        data=row.to_frame().T.iloc[:,:int(components[c][int(val)][2])]
                        date=datetime.datetime.strptime(row[1][0:6]+'20'+row[1][6:], '%d/%m/%Y-%H:%M:%S')
                        data[1]=date.replace(second=0)
                        if(len(c.split('_'))>1):#WMS Factor Multiplication
                            data[4]=(1.456*float(data[4].values[0]))-291.2
                        write(components[c][int(val)][0],components[c][int(val)][1],path_temp,data,date)
                        if(date.year<2015):
                            continue
                        with open(startpath, "w") as file:
                            file.write(str(time_now.date())+"\n"+str(time_now.time().replace(microsecond=0)))
                        with open("/home/admin/Start/MasterMail/IN-006W_FTPNewFiles.txt", "w") as file:
                            file.write(str(time_now.replace(microsecond=0)))
                print('Written!')
            cnt=cnt+1
        ftp.close()
        print('Sleeping')
    except:
        logging.exception('Main Failed')
    time.sleep(10800)

    