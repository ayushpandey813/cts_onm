
colnamesForWrite = ""
printMonthlyReport = function(year,x,months,y,stationName,path1G,MeterNo,path3G,colnoenergy)
{
	if(x == 1 && y < 3)
		return
	yr = prevyr = years[x]
	mn = months[y-1]
	{
	if( y > 2)
	{
		prevmn = months[y-2]
	}
	else
	{
		prevmn = dir(years[x-1])
		prevmn = prevmn[length(prevmn)]
		prevyr = years[x-1]
	}
	}
	path = paste(path1G,yr,mn,sep="/")
	pathprev = paste(path1G,prevyr,prevmn,sep="/")
	if(meterNo != "NoMeter")
	{
		path = paste(path,meterNo,sep="/")
		pathprev = paste(pathprev,meterNo,sep="/")
	}
	days = dir(path)
	daysprev = dir(pathprev)
	path = paste(path,days[length(days)],sep="/")
	pathprev = paste(pathprev,daysprev[length(daysprev)],sep="/")
	dataread1 = read.table(path,sep="\t",header = T)
	dataread2 = read.table(pathprev,sep="\t",header = T)
	r1 = as.numeric(dataread[complete.cases(dataread1[,colnoenergy]),colnoenergy])
	r2 = as.numeric(dataread[complete.cases(dataread2[,colnoenergy]),colnoenergy])
	val = as.character(abs(r1[length(r1)] - r2[length(r2)]))
	pathwrite = paste(path3G,"/",stationName," ",yr,".txt",sep="")
	colappen = 0
	{
		if(meterNo = "NoMeter")
		{
			if(length(colnamesForWrite)<1)
				colnamesForWrite <<- c("Month","MonthlyEnergy")
			df = data.frame(x = mn,val=val)
		}
		else 
		{
			if(length(colnamesForWrite) < 1)
			colnamesForWrite <<- c("Month","MonthlyEnergyM1","MonthlyEnergyM2")
			{
			if(grepl(days[length(days)],"M1"))
			{
				colappend = 2
			}
			else
			{
				colappend = 3
			}
			}
			colnamesForWrite[colappend] <<- paste("MonthlyEnergy",meterNo,sep="")
			}
	}
		if(!file.exists(pathwrite))
		{
			{
				if(colappend == 0)
				{
					colnames(df) = colnamesForWrite
					write.table(df,file=pathwrite,col.names=T,row.names=F,sep = "\t",append = F)
				}
				else if(colappend ==2)
				{
					df = data.frame(x=mn,val1=val,val2="NA")
					colnames(df) = colnamesForWrite
					write.table(df,file=pathwrite,col.names=T,row.names=F,sep = "\t",append = F)
				}
				else if(colappend ==3)
				{
					df = data.frame(x=mn,val1="NA",val2=val)
					colnames(df) = colnamesForWrite
					write.table(df,file=pathwrite,col.names=T,row.names=F,sep = "\t",append = F)
				}
			}
		}
		else
		{
			if(colappend == 0)
			{
				write.table(df,file=pathwrite,col.names = F,row.names=F,sep = "\t",append = T)
			}
			else
			{
				dataread = read.table(pathwrite,header =T,sep="\t")
				mtch = match(as.character(mn),as.character(dataread[,1]))
				{
				if(is.finite(mtch))
				{
					dataread[mtch,colappend]=val
					write.table(dataread,file=pathwrite,col.names = F,row.names=F,sep = "\t",append = T)
				}
				else
				{
					df = data.frame(x=mn,val1="NA",val2="NA")
					colnames(df)=colnamesForWrite
					df[1,colappend] = val
					write.table(df,file=pathwrite,col.names = F,row.names=F,sep = "\t",append = T)
				}
				}
			}
		}
}
