bucketTime = function(time)
{
	hr = as.numeric(substr(time,12,13))
	min = as.numeric(substr(time,15,16))
	bucket =(floor((((hr*60) + min)/5)) + 1)
	return(bucket)
}
postAnalysis = function(subdata)
{
	colCount = ncol(subdata)
	rowCount = nrow(subdata)
	rowVal = c()
	for(x in 1 : colCount)
	{
		rowVal[x] = subdata[1,x]
		for(y in 1 : rowCount)
		{
			if(!is.na(subdata[y,x]))
			{
				rowVal[x] = subdata[y,x]
				break
			}
		}
	}
	return(rowVal)
}

rewriteGen1 = function(gen1path,gen1cleanpath)
{
	dataread = read.table(gen1path,header = T,sep = "\t")
	dataread = dataread[complete.cases(as.character(dataread[,1])),]
	timebuckets = bucketTime(as.character(dataread[,1]))
	idx = match(unique(timebuckets),timebuckets)
	datawrite = dataread[idx,]
	if(length(idx) != length(timebuckets))
	{
		dup = timebuckets[duplicated(timebuckets)]
		for(t in 1 : length(dup))
		{
			idxs = which(timebuckets %in% dup[t])
			subdata = dataread[idxs,]
			datawrite[idxs[1],] = postAnalysis(subdata)
		}
	}
	write.table(datawrite,file = gen1cleanpath,colnames = T,rownames = F,sep="\t",append = F)
}
