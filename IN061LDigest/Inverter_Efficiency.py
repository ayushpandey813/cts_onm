import os
from datetime import timedelta 
import datetime as DT
import sys
import pandas as pd
today = DT.date.today()
yesterday = today - DT.timedelta(days=1)
today = str(today)
year = today[0:4]
month = today[0:7]
inv_eff_list = []

system_size = [74100, 64000, 74100, 62400, 62400]
inv_list = ['INVERTER_1', 'INVERTER_2', 'INVERTER_3', 'INVERTER_4', 'INVERTER_5']
stn = '[IN-061L]'

def Inv_eff(path,i):
  df11 = pd.read_csv(path,sep='\t')
  df11.dropna(how='any') 
  df11=(df11[['ts','W_avg','DCW_avg']].dropna())
  df11=df11.loc[((df11['W_avg']>0) & (df11['DCW_avg']>df11['W_avg'])) ,] #Filters
  df11['Loading'] = df11['W_avg']/system_size[i]
  df11['Eff'] = df11['W_avg']/df11['DCW_avg']
  df11['sumpro1'] = df11['Loading']*df11['Eff']
  sp = df11['sumpro1'].sum()
  sl = df11['Loading'].sum()
  inv_eff11 = round((sp/sl)*100,2)
  inv_eff_list.append(inv_eff11)

for i in range(len(inv_list)):
  if len(inv_list) < 9:	
    file_name = inv_list[i][0] + inv_list[i][9]	
  elif len(inv_list) < 99:
    file_name = inv_list[i][0] + inv_list[i][9:11]
  else:
    file_name = inv_list[i][0] + inv_list[i][9:12]
  Inv_eff('/home/admin/Dropbox/Gen 1 Data/'+stn+'/'+year+'/'+month+'/'+inv_list[i]+'/'+stn+'-'+file_name+'-'+today+'.txt' ,i)


print(inv_eff_list)