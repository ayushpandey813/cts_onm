rm(list=ls(all =TRUE))
pathRead <- "~/intern/Data/[716]/"

setwd(pathRead)
pathwritetxt <- "C:/Users/talki/Desktop/cec intern/results/716/uptime/[SG-003]_summary 1.txt"
pathwritecsv <- "C:/Users/talki/Desktop/cec intern/results/716/uptime/[SG-003]_summary 1.csv"

rf = function(x)
{
  return(format(round(as.numeric(x),2),nsmall=2))
}

filelist <- dir(pattern = ".txt", recursive= TRUE)
timemin <- format(as.POSIXct("2017-01-10 06:59:59"), format="%H:%M:%S")
timemax <- format(as.POSIXct("2017-11-06 17:00:00"), format="%H:%M:%S")
#col 20/67/114
ac1 <- 20
ac2 <- 67
ac3 <- 114
stationLimitm1 <- 839.52
stationLimitm2 <- 782.28
stationLimitm3 <- 165.36

nameofStation <- "SG-003"

col0 <- c()
col1 <- c()
col2 <- c()
col3 <- c()
col4 <- c()
col5 <- c()
col6 <- c()
col7 <- c() #up2
col8 <- c() #wup2
col9 <- c() #pr2
col10 <- c()  #up3
col11 <- c()  #wup3
col12 <- c()  #pr3
col13 <- c()
col0[10^6] = col1[10^6] = col2[10^6] = col3[10^6] = col4[10^6] = col5[10^6] = col6[10^6] =  col7[10^6]= col8[10^6]= col9[10^6]= col10[10^6]=col11[10^6]=col12[10^6]=col13[10^6]=0
index <- 1

for (i in filelist[56:length(filelist)]){
    data <- NULL
    temp <- read.table(i,header = T, sep = "\t")
    date <- substr(i,20,29)
    col0[index] <- nameofStation
    col1[index] <- date
    
    #data availability
    pts <- length(temp[,1])
    col3[index] <- pts
    
    condition <- format(as.POSIXct(temp[,1]), format="%H:%M:%S") > timemin &
      format(as.POSIXct(temp[,1]), format="%H:%M:%S") <timemax
    pacTemp <- temp[condition,]
    irrad <- as.numeric(paste(abs(pacTemp[,3])))
    pacTemp1 <- pacTemp[complete.cases(as.numeric(paste(abs(pacTemp[,ac1])))),]
    pacTemp2 <- pacTemp[complete.cases(as.numeric(paste(abs(pacTemp[,ac2])))),]
    pacTemp3 <- pacTemp[complete.cases(as.numeric(paste(abs(pacTemp[,ac3])))),]
    
    #pac uptime M1    
    pac1 <- as.numeric(paste(abs(pacTemp1[,ac1])))
    denom <- length(pac1)
    flag <- pac1<0.1
    flagRate <- length(flag[!flag]) / denom
    #flagRate <- numer/denom
    col4[index] <- flagRate*100
    #wpac M1
    gsi <- as.numeric(paste(abs(pacTemp1[,3])))
    flag <- as.numeric(pac1<0.1 & abs(gsi) > 20)
    wgsi <- gsi/sum(gsi)
    downloss <- wgsi * flag * 100
    if (length(downloss)>0){
      col5[index] <- 100 - sum(downloss)
    }
    else{
      col5[index] <- NA
    }
    
    #pac m2
    pac2 <- as.numeric(paste(abs(pacTemp2[,ac2])))
    denom <- length(pac2)
    flag2 <- pac2<0.1
    flagRate <- length(flag2[!flag2]) / denom
    col7[index] <- flagRate*100
    #wpac m2
    gsi2 <- as.numeric(paste(abs(pacTemp2[,3])))
    flag2 <- as.numeric(pac2<0.1 & abs(gsi2) > 20)
    wgsi2 <- gsi2/sum(gsi2)
    downloss <- wgsi2 * flag2 * 100
    if (length(downloss)>0){
      col8[index] <- 100 - sum(downloss)
    }
    else{
      col8[index] <- NA
    }
    
    #pac m3
    pac3 <- as.numeric(paste(abs(pacTemp3[,ac3])))
    denom <- length(pac3)
    flag3 <- pac3<0.1
    flagRate <- length(flag3[!flag3]) / denom
    col10[index] <- flagRate*100
    #wpac m3
    gsi3 <- as.numeric(paste(abs(pacTemp3[,3])))
    flag3 <- as.numeric(pac3<0.1 & abs(gsi3) > 20)
    wgsi3 <- gsi3/sum(gsi3)
    downloss <- wgsi3 * flag3 * 100
    if (length(downloss)>0){
      col11[index] <- 100 - sum(downloss)
    }
    else{
      col11[index] <- NA
    }
    
    #pr1
    eacTemp1 <- temp[complete.cases(as.numeric(paste(temp[,36]))),]  #removing NAs
    #eacTemp <- temp[complete.cases(as.numeric(paste(temp[,42]))),]
    eac1 <- as.numeric(paste(eacTemp1[,36]))
    #gsi <- as.numeric(paste(eacTemp[,3]))
    diff1 <- eac1[length(eac1)]- eac1[1]
    pr1 <- diff1/stationLimitm1/sum(gsi)*60000
    if (length(pr1)==0){
      col6[index] = NA
    }
    else{
      col6[index] <- pr1*100
    }
    
    #pr2
    eacTemp2 <- temp[complete.cases(as.numeric(paste(temp[,83]))),]  #removing NAs
    eac2 <- as.numeric(paste(eacTemp2[,83]))
    diff2 <- eac2[length(eac2)]- eac2[1]
    pr2 <- diff2/stationLimitm2/sum(gsi2)*60000
    if (length(pr2)==0){
      col9[index] = NA
    }
    else{
      col9[index] <- pr2*100
    }
    #pr_total
    
    difft <- diff1+diff2
    prt <- difft/1621.8/sum(irrad)*60000
    #col13[index] <- prt*100
    if (length(prt)==0){
      col13[index] = NA
    }
    else{
      col13[index] <- prt*100
    }
    #pr3
    eacTemp3 <- temp[complete.cases(as.numeric(paste(temp[,130]))),]  #removing NAs
    eac3 <- as.numeric(paste(eacTemp3[,130]))
    diff3 <- eac3[length(eac3)]- eac3[1]
    pr3 <- diff3/stationLimitm3/sum(gsi3)*60000
    if (length(pr3)==0){
      col12[index] = NA
    }
    else{
      col12[index] <- pr3*100
    }
    
    print(paste(i, "done"))
    
    index <- index + 1
}


col0 <- col0[1:(index-1)]
col1 <- col1[1:(index-1)]
col2 <- col2[1:(index-1)]
col3 <- col3[1:(index-1)]
col4 <- col4[1:(index-1)]
col5 <- col5[1:(index-1)]
col6 <- col6[1:(index-1)]
col7 <- col7[1:(index-1)]
col8 <- col8[1:(index-1)]
col9 <- col9[1:(index-1)]
col10 <- col10[1:(index-1)]
col11 <- col11[1:(index-1)]
col12 <- col12[1:(index-1)]
col13 <- col13[1:(index-1)]

col2 <- col3/max(col3)*100

col2[is.na(col2)] <- NA
col3[is.na(col3)] <- NA
col4[is.na(col4)] <- NA
col5[is.na(col5)] <- NA
col6[is.na(col6)] <- NA
col7[is.na(col7)] <- NA
col8[is.na(col8)] <- NA
col9[is.na(col9)] <- NA
col10[is.na(col10)] <- NA
col11[is.na(col11)] <- NA
col12[is.na(col12)] <- NA
col13[is.na(col13)] <- NA

col12[col12>100 |col12<0] <- NA
col9[col9>100 | col9<0] <- NA
col6[col6>100 | col6<0] <- NA

result <- cbind(col0,col1,col2,col3,col4,col5,col6,col7,col8,col9,col10,col11,col12,col13)
result[,3] <- round(as.numeric(result[,3]),1)
result[,5] <- round(as.numeric(result[,5]),1)
result[,6] <- round(as.numeric(result[,6]),1)
result[,7] <- round(as.numeric(result[,7]),1)
result[,8] <- round(as.numeric(result[,8]),1)
result[,9] <- round(as.numeric(result[,9]),1)
result[,10] <- round(as.numeric(result[,10]),1)
result[,11] <- round(as.numeric(result[,11]),1)
result[,13] <- round(as.numeric(result[,13]),1)
result[,14] <- round(as.numeric(result[,14]),1)


rownames(result) <- NULL
result <- data.frame(result)
dataWrite <- result
colnames(result) <- c("Meter Reference","Date","Data Availability [%]",
                      "No. of Points", "Uptime M1 [%]","Weighted Uptime M1 [%]",
                      "Performance Ratio [%]",
                      "Uptime M2 [%]","Weighted Uptime M2 [%]",
                      "Performance Ratio M2 [%]",
                      "Uptime M3 [%]","Weighted Uptime M3 [%]",
                      "Performance Ratio M3 [%]", 
                      "Performance Ratio M1-M2 [%]")
write.table(result,na = "",pathwritetxt,row.names = FALSE,sep ="\t")
write.csv(result,na = "",pathwritecsv,row.names = FALSE)

