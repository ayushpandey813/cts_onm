rm(list = ls())
TIMESTAMPSALARM = NULL
ltcutoff = .030
instcaP = .63
timetomins = function(x)
{
  hr = unlist(strsplit(x,":"))
	seq1 = seq(from = 1,to = length(hr),by =2)
	seq2 = seq(from = 2,to = length(hr),by =2)
  min = as.numeric(hr[seq2]) 
  hr = as.numeric(hr[seq1]) * 60
  return((hr + min + 1))
}
checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}
secondGenData = function(filepath,writefilepath)
{
	prdebug=0

	dt=totrowsmissing=Eac12=Eac13=Eac11=Eac21=Eac22=percpumpedin=solarconsumed=NA
	solarconsumedperc=totconsumed=irr=pr1=pr2=dspy=NA

	TIMESTAMPSALARM <<- NULL
  dataread = read.table(filepath,header = T,sep = "\t")
	datareadog = dataread
	dataread2 = dataread
	if(length(dataread2[complete.cases(dataread2[,3]),3]))
		irr = format(round(sum(dataread2[complete.cases(dataread2[,3]),3])/60000,2),nsmall=2)
	dataread = dataread2[complete.cases(dataread2[,39]),]
	if(nrow(dataread))
  	Eac11 = format(round(as.numeric(dataread[nrow(dataread),39]) - as.numeric(dataread[1,39]),1),nsmall=1)
	dataread = dataread2[complete.cases(dataread2[,24]),]
  dataread = dataread[as.numeric(dataread[,24])>0,]
	if(nrow(dataread))
		Eac12 = format(round(sum(as.numeric(dataread[,24]))/60,1),nsmall=1)
	dataread = dataread2[complete.cases(dataread2[,24]),]
  dataread = dataread[as.numeric(dataread[,24])<0,]
	if(nrow(dataread))
		Eac13 = format(round(abs(sum(as.numeric(dataread[,24]))/60),1),nsmall=1)
	dataread = dataread2[complete.cases(dataread2[,40]),]
  if(nrow(dataread))
		Eac21 = format(round(as.numeric(dataread[nrow(dataread),40]) - as.numeric(dataread[1,40]),1),nsmall=1)
	dataread = dataread2[complete.cases(dataread2[,71]),]
  if(nrow(dataread))
		Eac22 = format(round(sum(as.numeric(dataread[,71]))/60,1),nsmall=1)
	pr1 = round((as.numeric(Eac22)/(instcaP * as.numeric(irr))),1)
	dspy1 = round((as.numeric(Eac22)/(instcaP * 100)),1)
  dataread =  dataread2
	dataread = dataread[complete.cases(dataread[,86]),]
  lr = NA
  lr2 = NA
  lt = NA
	if(nrow(dataread))
	{
		pr2 = round(((as.numeric(dataread[nrow(dataread),86]) - as.numeric(dataread[1,86]))/(instcaP * as.numeric(irr))),1)
		dspy =  round(((as.numeric(dataread[nrow(dataread),86]) - as.numeric(dataread[1,86]))/(instcaP * 100)),2)
    lr = as.numeric(dataread[nrow(dataread),86])
	lr2 = as.numeric(dataread[nrow(dataread),40])
    lt = as.character(dataread[nrow(dataread),1])
	}
	dataread = dataread2
	DA = format(round(nrow(dataread)/14.4,1),nsmall=1)
  tdx = timetomins(substr(dataread[,1],12,16))
  dataread2 = dataread[tdx > 540,]
  tdx = tdx[tdx > 540]
  dataread2 = dataread2[tdx < 1020,]
	datareadNA = dataread2[(!complete.cases(dataread2[,71])),]
	dataread2 = dataread2[complete.cases(dataread2[,71]),]
  missingfactor = 480 - nrow(dataread2)
  dataread3 = dataread2[as.numeric(dataread2[,71]) < ltcutoff,]
	
	if(length(dataread3[,1]) > 0 )
		TIMESTAMPSALARM <<- as.character(dataread3[,1])
	if(length(datareadNA[,1]))
		TIMESTAMPSALARM <<- paste(TIMESTAMPSALARM,as.character(datareadNA[,1]))

	percpumpedin = format(round((as.numeric(Eac21)/as.numeric(Eac22)) * 100,1),nsmall=1)
	solarconsumed = as.numeric(Eac22) - as.numeric(Eac21)
	solarconsumedperc = 100 - as.numeric(percpumpedin)
	totconsumed = as.numeric(Eac11) + solarconsumed
  totrowsmissing = format(round((100-((missingfactor + nrow(dataread3))/4.8)),1),nsmall=1)
	dt = substr(as.character(datareadog[1,1]),1,10)
	if(prdebug)
	{
	print("==============================")
	print(paste("Date",dt))
	print(paste("totrow",totrowsmissing))
	print(paste("Eac12",Eac12))
	print(paste("Eac13",Eac13))
	print(paste("Eac11",Eac11))
	print(paste("Eac21",Eac21))
	print(paste("Eac22",Eac22))
	print(paste("Percin",percpumpedin))
	print(paste("solcons",solarconsumed))
	print(paste("solconsperc",solarconsumedperc))
	print(paste("Totcons",totconsumed))
	print(paste("Irr",irr))
	print(paste("PR1",pr1))
	print(paste("PR2",pr2))
	print(paste("DSPY",dspy))
	print("==============================")
 	}
	df = data.frame(Date = dt, DA=DA, DowntimeM2=totrowsmissing,Eac1LoadConsumed = as.numeric(Eac12),
	Eac1LoadProduction=as.numeric(Eac13),EnergyDelivered = as.numeric(Eac11),
	EnergyReceived = as.numeric(Eac21),Eac2SystGen = as.numeric(Eac22),
	PercPumpedInGrid=percpumpedin,SolarLoadConsumption=solarconsumed,
	SolarLoadConsumedPerc=solarconsumedperc,TotLoadConsumption=totconsumed,
	Irradiation = irr, PR1 = pr1, PR2 = pr2,DailySpecificYield2=dspy,DailySpecificYield1=dspy1,
  LastRead = lr,LastRead2 = lr2, LastTime = lt, stringsAsFactors=F)
  write.table(df,file = writefilepath,row.names = F,col.names = T,sep="\t",append = F)
  return(df)
}

thirdGenData = function(filepathm1,writefilepath)
{
  dataread1 =read.table(filepathm1,header = T,sep="\t")
  df = data.frame(Date = substr(as.character(dataread1[1,1]),1,10),
	TotPowerMeter2 = as.numeric(dataread1[,8]),
	PowerIntoGrid = as.numeric(dataread1[,7]),
	SolarPowerConsumedLoad=as.numeric(dataread1[,10]),
	PercSolarIntoGrid = as.numeric(dataread1[,9]),
	PercSolarConsumedLoad=as.numeric(dataread1[,11]),
	TotLoadConsumed=as.numeric(dataread1[,12]),
	Irradiation = as.numeric(dataread1[,13]),
	PR1 =as.numeric(dataread1[,14]),
	PR2 = as.numeric(dataread1[,15]),
	LastRead =as.numeric(dataread1[,18]),
	LastTime =(dataread1[,20]),
	DA =as.numeric(dataread1[,2]),
	LastRead =as.numeric(dataread1[,19]),
	stringsAsFactors=F)

  {
    if(!file.exists(writefilepath))
    {
      write.table(df,file = writefilepath,row.names = F,col.names = T,sep = "\t",append = F)
    }
    else
    {
		  dataread = read.table(writefilepath,header = T,sep="\t")
			idxmtch = match(as.character(df[,1]),as.character(dataread[,1]))
			{
			if(is.finite(idxmtch))
			{
				dataread[idxmtch,] = df[1,]
				write.table(dataread,file=writefilepath,row.names=F,col.names=T,sep="\t",append = F)
			}
			else{
      write.table(df,file = writefilepath,row.names = F,col.names = F,sep = "\t",append = T)
    }
		}
	 }
  }
  return(c(as.numeric(df[,2]),as.numeric(df[,3]),as.numeric(df[,4]),as.numeric(df[,6])))
}

