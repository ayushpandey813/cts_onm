errHandle = file('/home/admin/Logs/LogsKH003Mail.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)
source('/home/admin/CODE/KH003Digest/HistoricalAnalysis2G3GKH003.R')
source('/home/admin/CODE/Misc/memManage.R')
source('/home/admin/CODE/MasterMail/timestamp.R')
source('/home/admin/CODE/Send_mail/sendmail.R')
source('/home/admin/CODE/KH003Digest/aggregateInfo.R')
initDigest = function(df,no)
{
  {
	if(as.numeric(no) == 1)
	{
	  no2 = 'Solar'
	}
	else if(as.numeric(no)==2)
	{
	  no2 = 'Substation'
	}
	else if(as.numeric(no)==3)
	{
		no2 = "Factory Loads"
	}
	}
	ratspec = round(as.numeric(df[,2])/instCap,2)
  body = "\n\n__________________________________________________\n\n"
  body = paste(body,as.character(df[,1]),no2,sep=" ")
  body = paste(body,"\n\n__________________________________________________\n\n",sep="")
  body = paste(body,"Eac",no,"-1 [kWh]: ",as.character(df[,2]),sep="")
  body = paste(body,"\n\nEac",no,"-2 [kWh]: ",as.character(df[,3]),sep="")
	if(no == 3)
		body = paste(body,"\n\nExports Amount [kWh]: ",as.character(df[,15]),sep="")
	if(no==2)
  	body = paste(body,"\n\nCable losses [%]: ",CABLOSSTOPRINT,sep="")
  if(as.numeric(no)!=3)
	{
	body = paste(body,"\n\nYield-1 [kWh/kWp]: ",as.character(df[,11]),sep="")
	body = paste(body,"\n\nYield-2 [kWh/kWp]: ",as.character(df[,12]),sep="")
	body = paste(body,"\n\nPR",no2," [%]: ",as.character(df[,10]),sep="")
	}
  body = paste(body,"\n\nRatio [%]: ",as.character(df[,4]),sep="")
  acpts = round(as.numeric(df[,5]) * 14.4)
  body = paste(body,"\n\nPoints recorded: ",acpts," (",as.character(df[,5]),"%)",sep="")
  if(as.numeric(no)!=3)
	{
	body = paste(body,"\n\nDowntime (%): ",as.character(df[,6]),sep="")
	}
	body = paste(body,"\n\nLast recorded timestamp: ",as.character(df[,7]),sep="")
  body = paste(body,"\n\nLast recorded energy meter reading [kWh]: ",as.character(df[,8]),sep="")
  return(body)
}
printtsfaults = function(TS,num,body)
{
  {
	if(as.numeric(num) == 1)
	{
	  num = 'Solar'
	}
	else if(as.numeric(num)==2)
	{
	  num = 'Substation'
	}
	else if(as.numeric(num)==3)
	{
		num='Factory-Loads'
	}
	}
	if(length(TS) > 1)
	{
		body = paste(body,"\n\n__________________________________________________\n")
		body = paste(body,paste("\nTimestamps for",num,"where Pac < 1 between 8am -5pm\n"))
		for(lm in  1 : length(TS))
		{
			body = paste(body,TS[lm],sep="\n")
		}
	}
	return(body)
}
sendMail = function(df1,df2,pth1,pth2,pth3,df3,pth4)
{
  filetosendpath = c(pth1,pth2)
  if(file.exists(pth3))
	{
	filetosendpath = c(pth1,pth2,pth3)
  }
  if(file.exists(pth4))
	{
	filetosendpath[(length(filetosendpath)+1)] = pth4
  }
	filenams = c()
  for(l in 1 : length(filetosendpath))
  {
    temp = unlist(strsplit(filetosendpath[l],"/"))
    filenams[l] = temp[length(temp)]
		if(l == 1)
			currday = temp[length(temp)]
  }
	data3G = read.table(pth3,header =T,sep = "\t")
	dateineed = unlist(strsplit(filenams[1]," "))
	dateineed = unlist(strsplit(dateineed[2],"\\."))
	dateineed = dateineed[1] 
	idxineed = match(dateineed,as.character(data3G[,1]))
	irrsrc = "KH-003S"
	if(is.finite(idxineed))
		irrsrc = as.character(data3G[idxineed,32])
	print('Filenames Processed')
  body = ""
	body = paste(body,"Site Name: Coca-Cola Cambodia",sep="")
	body = paste(body,"\n\nLocation: Phnom Penh, Cambodia",sep="")
	body = paste(body,"\n\nO&M Code: KH-003",sep="")
	body = paste(body,"\n\nSystem Size: ",instCap," kWp",sep="")
	body = paste(body,"\n\nNumber of Energy Meters: 2",sep="")
	body = paste(body,"\n\nModule Brand / Model / Nos: Canadian Solar / CS6X-320P / 8200",sep="")
	body = paste(body,"\n\nInverter Brand / Model / Nos: SMA / STP60-10 / 36",sep="")
	body = paste(body,"\n\nSite COD: ",as.character(DOB),sep="")
	body = paste(body,"\n\nSystem age [days]: ",as.character((as.numeric(DAYSALIVE))),sep="")
	body = paste(body,"\n\nSystem age [years]: ",as.character(round((as.numeric(DAYSALIVE))/365,2)),sep="")
	body = paste(body,"\n\nTotal daily irradiation (from ",irrsrc,") [kWh/m2]: ",as.character(df3[,12]),sep="")
	body = paste(body,"\n\nTotal daily irradiation (from KH-714S) [kWh/m2]: ",as.character(df3[,17]),sep="")
	sdpi = round(as.numeric(df3[,12])/as.numeric(df3[,17]),2)
	body = paste(body,"\n\nVariation of irradiation: ",as.character(sdpi),sep="")
	PRLoss = round((as.numeric(df1[,12])*100/as.numeric(df3[,12])),1)
	body = paste(body,"\n\nTrue PR on site [%]: ",PRLoss,sep="")
	body = paste(body,"\n\nTheoretical PR loss on site [%]: ",round((80-PRLoss),1),sep="")
	body = paste(body,"\n\nTheoretical energy generation potential lost [kWh]: ",
	round((80-PRLoss)*instCap*(as.numeric(df3[,12]))/100,1),sep="")
	artLoad = (as.numeric(df1[,2])+as.numeric(df3[,2]))
	solCont = round(as.numeric(df1[,2]) * 100/artLoad,1)
	body = paste(body,"\n\nArtificial load [kWh]: ",artLoad,sep="")
	body = paste(body,"\n\nSolar contribution [%]: ",solCont,sep="")
	body = paste(body,"\n\nExports Amount [kWh]: ",as.character(df3[,15]),sep="")
	expShow = NA
	idmtch = match(as.character(df3[,1]),as.character(data3G[,1]))
	if(is.finite(idmtch))
		expShow = as.character(data3G[idmtch,31])
	body = paste(body,"\n\nExports Percentage [%]: ",expShow,sep="")

	body = paste(body,initDigest(df2,1))  #its correct, dont change
	body = paste(body,initDigest(df1,2))  #its correct, dont change
	if(file.exists(pth4))
		body = paste(body,initDigest(df3,3))
  body = printtsfaults(TIMESTAMPSALARM,1,body)
	print('2G data processed')
	body = paste(body,"\n\n__________________________________________________\n\n",sep="")
  body = paste(body,"Station Data",sep="")
  body = paste(body,"\n\n__________________________________________________\n\n",sep="")
  body = paste(body,"Station DOB: ",as.character(DOB),sep="")
  body = paste(body,"\n\n# Days alive: ",as.character(DAYSALIVE),sep="")
  yrsalive = format(round((DAYSALIVE/365),2),nsmall=2)
  body = paste(body,"\n\n# Years alive: ",yrsalive,sep="")
	body = gsub("\n ","\n",body)
	print('3G data processed')
	dateAc = substr(currday,14,23)
	#if(weekdays(as.Date(substr(currday,13,23))) == "Monday")
	{
		command = 'Rscript "/home/admin/CODE/JasonGraphs/KH-003X/[715_KH-003] Graphs.R"'
		system(command)
		command = 'Rscript "/home/admin/CODE/JasonGraphs/KH-003X/[KH-003X] Cable losses and irradiance plot.R"'
		system(command)
		command = 'Rscript "/home/admin/CODE/JasonGraphs/KH-003X/[KH-003X] Cable losses and irradiance plot.R"'
		system(command)
		command = 'Rscript "/home/admin/CODE/JasonGraphs/715/715 Data_extract.R"'
		system(command)
		command = 'Rscript "/home/admin/CODE/JasonGraphs/715/715 Graphs.R"'
		system(command)
		command = 'Rscript "/home/admin/CODE/JasonGraphs/715/Coke data extract and PR plot.R"'
		system(command)
		filetosendpath = c("/home/admin/Jason/cec intern/results/715/[DSPCoke] PR.pdf",
		"/home/admin/Jason/cec intern/results/715/[715]_summary.txt",
		filetosendpath,
		"/home/admin/Jason/cec intern/results/KH-003X/[KH-003]_PR_LC.pdf",
		"/home/admin/Jason/cec intern/results/KH-003X/[KH-003]715_WPAC_LC.pdf",
		"/home/admin/Jason/cec intern/results/KH-003X/[KH-003]_PAC_LC.pdf",
		"/home/admin/Jason/cec intern/results/KH-003X/[KH-003]715_DA_LC.pdf",
		"/home/admin/Jason/cec intern/results/KH-003X/KH-003_Cableloss.txt",
		"/home/admin/Jason/cec intern/results/KH-003X/irradiance filter plot 2.pdf",
		"/home/admin/Jason/cec intern/results/KH-003X/KH-003_CableLoss1.pdf",
		"/home/admin/Jason/cec intern/results/KH-003X/KH-003_CableLoss2.pdf",
		"/home/admin/Jason/cec intern/results/715/[715]_summary_PR_w_filter.txt",
		"/home/admin/Jason/cec intern/results/715/PR Progression - KH-003.pdf",
		"/home/admin/Jason/cec intern/results/715/PR2.pdf",
		"/home/admin/Jason/cec intern/results/715/PR1.pdf",
		"/home/admin/Jason/cec intern/results/715/[715]_summary.csv",
		"/home/admin/Jason/cec intern/results/715/[715]_summary_PR_m1m2.txt"
		)
		filenams = c(paste("[KH-003] Graph",dateAc,"- PR Evolution.pdf"),
		paste("[KH-003] Graph",dateAc,"- PR Evolution.txt"),
			filenams,
		"[KH-003]_PR_LC.pdf",
		"[KH-003]715_WPAC_LC.pdf",
		"[KH-003]_PAC_LC.pdf",
		"[KH-003]715_DA_LC.pdf",
		"KH-003_Cableloss.txt",
		"irradiance filter plot 2.pdf",
		"KH-003_CableLoss1.pdf",
		"KH-003_CableLoss2.pdf",
		"[715]_summary_PR_w_filter.txt",
		"PR Progression - KH-003.pdf",
		"PR2.pdf",
		"PR1.pdf",
		"[715]_summary.csv",
		"[715]_summary_PR_m1m2.txt"
		)
	}
  send.mail(from = sender,
            to = recipients,
            subject = paste("Station [KH-003X] Digest",dateAc),
            body = body,
            smtp = list(host.name = "smtp.office365.com", port = 587, user.name = uname, passwd = pwd, tls = TRUE),
            authenticate = TRUE,
            send = TRUE,
            attach.files = filetosendpath,
            file.names = filenams, # optional parameter
            debug = F)
recordTimeMaster("KH-003X","Mail",substr(currday,13,23))
}
sender = c('operations@cleantechsolar.com')
uname = 'shravan.karthik@cleantechsolar.com'

# recipients = getRecipients("KH-003X","m")
recipients = c('operationsKH@cleantechsolar.com','om-it-digest@cleantechsolar.com','om-interns@cleantechsolar.com','bunhak@teams-cambodia.com','vandy@teams-cambodia.com')
#recipients = c('shravan1994@gmail.com')

pwd = 'CTS&*(789'
todisp = 1
while(1)
{
	# recipients = getRecipients("KH-003X","m")
	recipients = c('operationsKH@cleantechsolar.com','om-it-digest@cleantechsolar.com','om-interns@cleantechsolar.com','bunhak@teams-cambodia.com','vandy@teams-cambodia.com')
  recordTimeMaster("KH-003X","Bot")
	sendmail = 0
  prevx = x
  prevy = y
  prevt = t
  years = dir(path)
  for(x in prevx : length(years))
  {
    pathyear = paste(path,years[x],sep="/")
    writepath2Gyr = paste(writepath2G,years[x],sep="/")
    writepath3Gyr = paste(writepath3G,years[x],sep="/")
    checkdir(writepath2Gyr)
    checkdir(writepath3Gyr)
    months = dir(pathyear)
    startmnth = 1
    if(prevx == x)
    {
      startmnth = prevy
    }
    for(y in startmnth : length(months))
    {
      pathmonths = paste(pathyear,months[y],sep="/")
      writepath2Gmon = paste(writepath2Gyr,months[y],sep="/")
      writepath3Gfinal = paste(writepath3Gyr,"/[KH-003X] ",months[y],".txt",sep="")
      checkdir(writepath2Gmon)
      stations = dir(pathmonths)
			if(length(stations) > 2)
			stations = c(stations[2],stations[3],stations[1])
      pathdays = paste(pathmonths,stations[1],sep="/")
      pathdays2 = paste(pathmonths,stations[2],sep="/")
      writepath2Gdays = paste(writepath2Gmon,stations[1],sep="/")
      writepath2Gdays2 = paste(writepath2Gmon,stations[2],sep="/")
      checkdir(writepath2Gdays)
      checkdir(writepath2Gdays2)
      days = dir(pathdays)
      days2 = dir(pathdays2)
			if(length(stations) > 2)
			{
      pathdays3 = paste(pathmonths,stations[3],sep="/")	
      writepath2Gdays3 = paste(writepath2Gmon,stations[3],sep="/")
      checkdir(writepath2Gdays3)
      days3 = dir(pathdays3)
			}
      startdays = 1
      if(y == prevy)
      {
        startdays = prevt
      }
      if(startdays == 1)
      {
        temp = unlist(strsplit(days[1]," "))
				print(substr(temp[2],1,10))
        temp = as.Date(substr(temp[2],1,10),"%Y-%m-%d")
      }
      for(t in startdays : length(days))
      {
         condn1  = (t != length(days))
         condn2 = ((t == length(days)) && ((y != length(months)) || (x != length(years))))
         if(!(condn1 || condn2))
         {
				 	 if(todisp)
					 {
					 	print('No new files')
						todisp = 0
					 }
           Sys.sleep(3600)
           next
         }
				 if(grepl("Copy",c(days[t],days2[t],days3[t])))
				 {
				 	daycop = c(days[t],days2[t],days3[t])
					daycop = daycop[grepl("Copy",daycop)]
					for(inner in 1 : length(daycop))
					{
				 	print('Copy file found so removing it')
					command = paste('rm "',pathdays,'/',daycop[t],'"',sep="")
					print(command)
					system(command)
					print('Command complete.. sleeping')
					}
				 	Sys.sleep(3600)
					next
				 }

				 if(is.na(days[t]) && is.na(days2[t]) && is.na(days3[t]))
				 {
					 	print('Files all NA restarting after hour')
           Sys.sleep(3600)
           next
				 }
				 todisp = 1
         sendmail = 1
         DAYSALIVE = DAYSALIVE + 1
				 print(paste('Processing',days[t]))
				 print(paste('Processing',days2[t]))
				 if(length(stations)>2)
				 print(paste('Processing',days3[t]))
         writepath2Gfinal = paste(writepath2Gdays,"/",days[t],sep="")
         writepath2Gfinal2 = paste(writepath2Gdays2,"/",days2[t],sep="")
				 writepath2Gfinal3 = df3=NULL
				 if(length(stations)>2)
         writepath2Gfinal3 = paste(writepath2Gdays3,"/",days3[t],sep="")

         readpath = paste(pathdays,days[t],sep="/")
         readpath2 = paste(pathdays2,days2[t],sep="/")
				 if(length(stations)>2)
         readpath3 = paste(pathdays3,days3[t],sep="/")

				 dataprev = read.table(readpath2,header = T,sep = "\t")
				 METERCALLED <<- 2  #its correct dont change
         df1 = secondGenData(readpath,writepath2Gfinal)
				 METERCALLED <<- 1 # its correct dont change
         df2 = secondGenData(readpath2,writepath2Gfinal2)
				 if(length(stations)>2)
				 {
				 METERCALLED <<- 3 # its correct dont change
         df3 = secondGenData(readpath3,writepath2Gfinal3)
				 }
				 datemtch = unlist(strsplit(days[t]," "))
				 {
				 if(length(stations)>2)
				 {
				 	thirdGenData(writepath2Gfinal,writepath2Gfinal2,writepath2Gfinal3,writepath3Gfinal)
					}
					else
				 	{
					thirdGenData(writepath2Gfinal,writepath2Gfinal2,NULL,writepath3Gfinal)
					}
				}

  if(sendmail ==0)
  {
    next
  }
	print('Sending Mail')
  sendMail(df1,df2,writepath2Gfinal,writepath2Gfinal2,writepath3Gfinal,df3,writepath2Gfinal3)
	print('Mail Sent')
      }
    }
  }
	gc()
}
sink()