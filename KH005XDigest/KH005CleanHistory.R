rm(list = ls())
errHandle = file('/home/admin/Logs/LogsKH005History.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)

require('mailR')
source('/home/admin/CODE/KH005XDigest/FTPKH005Dump.R')
source('/home/admin/CODE/Misc/memManage.R')
source('/home/admin/CODE/Send_mail/sendmail.R')
FIREERRATA = c(1,1,1,1,1)
LTCUTOFF = 0.001
checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}

timetomins = function(x)
{
  hrs = unlist(strsplit(x,":"))
  mins = as.numeric(hrs[2])
  hrs = as.numeric(hrs[1]) * 60
  return((((hrs + mins))+1))
}
NACOUNTER = FAULTCOUNTER = c(0,0,0,0,0)
THRESHNA=THRESHFAULT=20
checkErrata = function(row,ts,no)
{
	if(!(no==3))
		return()
	{
  if(no == 1)
		no2="ComAp-01"
	else if(no == 2)
		no2="MSB-01 Loads"
	else if(no==3)
	  no2 = 'Solar'
	else if(no==4)
		no2="ComAp-02"
	else if(no==5)
		no2="MSB-02 Loads"
	}
  if(ts < 540 || ts > 1020)
	{return()}
	if(FIREERRATA[no] == 0)
	{
	  print(paste('Errata mail already sent for meter',no,'so no more mails'))
		return()
	}
	{
	if(!is.finite(as.numeric(row[2])))
	{
		NACOUNTER[no]<<-NACOUNTER[no]+1
		if(NACOUNTER[no]<THRESHNA)
		{
			print(paste('Incremented NA values for mt',no,'but its still less than thresh'))
			return()
		}
	}
	else if((as.numeric(row[2]) > LTCUTOFF))
	{
		NACOUNTER[no]<<-0
		FAULTCOUNTER[no]<<-0
	}
	else
	{
		FAULTCOUNTER[no]<<-FAULTCOUNTER[no]+1
		if(FAULTCOUNTER[no]<THRESHFAULT)
		{
			print(paste('Incremented fault values for mt',no,'but its still less than thresh'))
			return()
		}
	}
	}
	if((!(is.finite(as.numeric(row[2])))) || (as.numeric(row[2]) < LTCUTOFF))
	{
		FIREERRATA[no] <<- 0
		subj = paste('KH-005X',no2,'down')
		body=""
		if(NACOUNTER[no]>=THRESHNA)
			body = paste(body,"Error description: Ten consecutive NA readings detected for Pac\n",sep="")
		if(FAULTCOUNTER[no]>=THRESHFAULT)
			body = paste(body,"Error description: Ten consecutive readings below threshold for Pac\n",sep="")
		body = paste(body,'\nTimestamp: ',as.character(row[1]),sep="")
		body = paste(body,'\n\nReal Power Tot kW reading: ',as.character(row[2]),sep="")
		send.mail(from = 'operations@cleantechsolar.com',
							to = getRecipients("KH-005X","a"),
							subject = subj,
							body = body,
							smtp = list(host.name = "smtp.office365.com", port = 587, user.name = 'shravan.karthik@cleantechsolar.com',passwd = 'CTS&*(789', tls = TRUE),
							authenticate = TRUE,
							send = TRUE,
							debug = F)
		print(paste('Errata mail sent meter ',no))
		msgbody = paste(subj,"\n Timestamp:",as.character(row[1]),
		                "\n Real Pow Tot kW reading:",as.character(row[2]))
		if(NACOUNTER[no]>=THRESHNA)
			msgbody = paste(msgbody,"\nTen consecutive NA readings")
		if(FAULTCOUNTER[no]>=THRESHFAULT)
			body = paste(body,"\nTen consecutive readings below threshold\n",sep="")
		command = paste('python /home/admin/CODE/Send_mail/twilio_alert.py',' "',msgbody,'"',' "KH-005X"',sep = "")    ########## CHANGE PYTHON SCRIPT TO REFLECT TRUE OWNERS AS ARUN IS REMOVED
		system(command)
		print('Twilio message fired')
		recordTimeMaster("KH-005X","TwilioAlert",as.character(row[1]))
		NACOUNTER[no]<<-0
		FAULTCOUNTER[no]<<-0
	}
}

stitchFile = function(path,days,pathwrite,erratacheck)
{
print('Inside stitchFile')
x = 1
t = try(read.csv(paste(path,days[x],sep="/"),header = T, skip=6,stringsAsFactors=F),silent = T)
  if(length(t)==1)
        {
                print(paste("Skipping",days[x],"Due to err in file"))
                return()
        }
  dataread = t
        if(nrow(dataread) < 1)
                return()
  dataread = dataread[,-c(1,2)]
	colnames(dataread)[1] = "Tm"
  currcolnames = colnames(dataread)
  #temp = unlist(strsplit(days[x]," "))
  #      temp = unlist(strsplit(temp[2],"_"))
  #temp = as.numeric(temp[1])a
	tmstmps = as.character(dataread[,1])
	tmstmps = unlist(strsplit(tmstmps," "))
	seqa = seq(from =1 ,to=length(tmstmps),by=2)
	tmstmps2 = tmstmps[seqa]
	tmstmps = tmstmps[(seqa+1)]
	tmstmps3 = unlist(strsplit(tmstmps2,"-"))
	seqb = seq(from=1,to=length(tmstmps3),by=3)
	tmstmps3 = paste(tmstmps3[(seqb+2)],tmstmps3[(seqb)],tmstmps3[(seqb+1)],sep="-")
	tmstmps = paste(tmstmps3,tmstmps)
	dataread[,1]=as.character(tmstmps)
	temp = substr(days[x],1,3)
	{
		if(temp=='Com')
		{	
			temp2 = "ComAp"
			temp = 1
			mtno = 1
			rowtempuse = rowtemp
		}
		else if(temp=='Gsi')
		{
			temp2 = "SCS"
			temp=2
			mtno = 3
			rowtempuse = rowtempc
		}
		else if(temp=='SCL')
		{
			temp2 = "SCL"
			temp=3
			mtno = 2
			rowtempuse = rowtempb
		}
		else if(temp=='SCS')
		{
			temp2 = "SCS"
			temp = 4
			mtno = 3
			rowtempuse = rowtempc
		}
		else if(temp=='Tem')
		{
			temp2 = "SCS"
			temp=5
			mtno = 3
			rowtempuse = rowtempc
		}
	}
	indexpass = 18
	colnamesuse = colnamesc
	{
		if(temp==2)
			currcolnames = c("Tm","Gsi")
		else if(temp == 5)
			currcolnames = c("Tm","Tamb")
	}

	if(temp == 1)
		colnamesuse = colnames
	if(temp == 3)
		colnamesuse = colnamesb

	idxvals = match(currcolnames,colnamesuse)
	idxvals = idxvals[complete.cases(idxvals)]

  for(y in 1 : nrow(dataread))
  {
    idxts = timetomins(substr(as.character(dataread[y,1]),12,16))
    if(!is.finite(idxts))
    {
      print(paste('Skipping row',y,'as incorrect timestamp'))
      next
    }
    yr = substr(as.character(dataread[y,1]),1,4)
    pathwriteyr = paste(pathwrite,yr,sep="/")
    checkdir(pathwriteyr)
    mon = substr(as.character(dataread[y,1]),1,7)
    pathwritemon = paste(pathwriteyr,mon,sep="/")
    checkdir(pathwritemon)
		pathwritefinal = paste(pathwritemon,temp2,sep="/")
    checkdir(pathwritefinal)
    day = substr(as.character(dataread[y,1]),1,10)
    filename = paste(stnname,mtno,"] ",day,".txt",sep="")
    pathtowrite = paste(pathwritefinal,filename,sep="/")
    rowtemp2 = rowtempuse
		print(paste('index is',idxvals))
		print('------------------------------')
		idxvals2 = idxvals
		if(temp==2 || temp==5)
			idxvals2 = c(1,2)
    rowtemp2[idxvals] = dataread[y,idxvals2]
		print(paste('Writing files',pathtowrite))
		{
   	 	if(!file.exists(pathtowrite))
    	{
      	df = data.frame(rowtemp2,stringsAsFactors = F)
      	colnames(df) = colnamesuse
      	if(idxts != 1)
        	{
          	df[idxts,] = rowtemp2
          	df[1,] = rowtempuse
        	}
				FIREERRATA[temp] <<- 1
				NACOUNTER[temp] <<- 0
				FAULTCOUNTER[temp] <<-0
    	}
    	else
    	{
      	df = read.table(pathtowrite,header =T,sep = "\t",stringsAsFactors=F)
      	df[idxts,idxvals] = rowtemp2[idxvals]
    	}
  	}
		if(erratacheck != 0 && temp==4)
		{
			passrow2 = df[idxts,indexpass]
			if(temp==3)
				passrow2 = abs(as.numeric(df[idxts,indexpass]))
			pass = c(as.character(df[idxts,1]),as.character(passrow2))
			checkErrata(pass,idxts,mtno)
		}
		df = df[complete.cases(df[,1]),]
		tdx = as.POSIXct(strptime(as.character(df[,1]), "%Y-%m-%d %H:%M:%S"))
		df = df[order(tdx),]
  	write.table(df,file = pathtowrite,row.names = F,col.names = T,sep = "\t",append = F)
  }
	recordTimeMaster("KH-005X","Gen1",as.character(df[nrow(df),1]))
}

path = '/home/admin/Data/Episcript-CEC/EGX KH005X Dump'
pathwrite = '/home/admin/Dropbox/Gen 1 Data/[KH-005X]'
pathdatelastread = '/home/admin/Start'
checkdir(pathdatelastread)
pathdatelastread = paste(pathdatelastread,'KH005.txt',sep="/")
days = dir(path)
days = days[grepl("csv",days)]
lastrecordeddate = c('','','','','')
idxtostart = c(1,1,1,1,1)
#idxtostart = c(1,2,3,4,5)
{
	if(!file.exists(pathdatelastread))
	{
		print('Last date read file not found so cleansing system from start')
		system('rm -R "/home/admin/Dropbox/Gen 1 Data/[KH-005X]"')
	}
	else
	{
		lastrecordeddate = readLines(pathdatelastread)
		lastrecordeddate = c(lastrecordeddate[1],lastrecordeddate[2],lastrecordeddate[3],
		lastrecordeddate[4],lastrecordeddate[5])
		idxtostart[1] = match(lastrecordeddate[1],days)
		idxtostart[2] = match(lastrecordeddate[2],days)
		idxtostart[3] = match(lastrecordeddate[3],days)
		idxtostart[4] = match(lastrecordeddate[4],days)
		idxtostart[5] = match(lastrecordeddate[5],days)
		print(paste('Date read is',lastrecordeddate[1],'and idxtostart is',idxtostart[1]))
		print(paste('Date read is',lastrecordeddate[2],'and idxtostart is',idxtostart[2]))
		print(paste('Date read is',lastrecordeddate[3],'and idxtostart is',idxtostart[3]))
		print(paste('Date read is',lastrecordeddate[4],'and idxtostart is',idxtostart[4]))
		print(paste('Date read is',lastrecordeddate[5],'and idxtostart is',idxtostart[5]))
	}
}

checkdir(pathwrite)
colnames = colnames(read.csv(paste(path,days[idxtostart[1]],sep="/"),header = T, skip=6,stringsAsFactors=T))
colnames = colnames[-c(1,2)]
colnamesb = colnames(read.csv(paste(path,days[idxtostart[3]],sep="/"),header = T, skip=6,stringsAsFactors=T))
colnamesb = colnamesb[-c(1,2)]
colnamesc = colnames(read.csv(paste(path,days[idxtostart[4]],sep="/"),header = T, skip=6,stringsAsFactors=T))
colnamesc= colnamesc[-c(1,2)]
colnamesc = c(colnamesc,"Gsi","Tamb")

colnames[1] = "Tm"
colnamesb[1] = "Tm"
colnamesc[1] = "Tm"

rowtemp = rep(NA,(length(colnames)))
rowtempb = rep(NA,(length(colnamesb)))
rowtempc = rep(NA,(length(colnamesc)))

x=1
stnname =  "[KH-005X-M"

days1 = days[grepl('Com',days)]
days2 = days[grepl('Gsi',days)]
days3 = days[grepl('SCL',days)]
days4 = days[grepl('SCS',days)]
days5 = days[grepl('Tem',days)]

offset = c(0)
offset[1]=0
offset[2]=offset[1] + length(days1)
offset[3]=offset[2]+length(days2)
offset[4]=offset[3]+length(days3)
offset[5]=offset[4]+length(days4)

{
	if(idxtostart[1] < length(days1))
	{
		print(paste('idxtostart[1] is',idxtostart[1],'while length of days1 is',length(days1)))
		for(x in idxtostart[1] : length(days1))
		{
 		 stitchFile(path,days1[x],pathwrite,0)
		}
		lastrecordeddate[1] = as.character(days1[x])
		print('Meter 1 DONE')
	}
	if((idxtostart[2]-offset[2]) < length(days2))
	{
		print(paste('idxtostart[2] is',idxtostart[2],'while length of days2 is',length(days2)))
		for(x in (idxtostart[2]-offset[2]) : length(days2))
		{
 		 stitchFile(path,days2[x],pathwrite,0)
		}
		lastrecordeddate[2] = as.character(days2[x])
		print('Meter 2 Done')
	}
	if((idxtostart[3]-offset[3]) < length(days3))
	{
		print(paste('idxtostart[3] is',idxtostart[3],'while length of days3 is',length(days3)))
		for(x in (idxtostart[3]-offset[3]) : length(days3))
		{
 		 stitchFile(path,days3[x],pathwrite,0)
		}
		lastrecordeddate[3] = as.character(days3[x])
		print('Meter 3 Done')
	}
	if((idxtostart[4] - offset[4])< length(days4))
	{
		print(paste('idxtostart[4] is',idxtostart[4],'while length of days4 is',length(days4)))
		for(x in (idxtostart[4]-offset[4]) : length(days4))
		{
 		 stitchFile(path,days4[x],pathwrite,0)
		}
		lastrecordeddate[4] = as.character(days4[x])
		print('Meter 4 Done')
	}
	if((idxtostart[5] - offset[5])< length(days5))
	{
		print(paste('idxtostart[5] is',idxtostart[5],'while length of days5 is',length(days5)))
		for(x in (idxtostart[5] - offset[5]): length(days5))
		{
 		 stitchFile(path,days5[x],pathwrite,0)
		}
		lastrecordeddate[5] = as.character(days5[x])
		print('Meter 5 Done')
	}
	else if( (!(idxtostart[1] < length(days1))) && (!(idxtostart[2] < length(days2))) && (!(idxtostart[3])<length(days3)) &&
	 (!(idxtostart[4] < length(days4))) && (!(idxtostart[5] < length(days5))) )
	{
		print('No historical backlog')
	}
}

print('_________________________________________')
print('Historical Calculations done')
print('_________________________________________')
reprint = 1
print(lastrecordeddate)
write(lastrecordeddate,pathdatelastread)
print('File Written')
ERRATAFIX=0
while(1)
{
  print('Checking FTP for new data')
  filefetch = dumpftp(days,path)
	if(filefetch == 0)
	{
		Sys.sleep(10800)
		next
	}
	print('FTP Check complete')
	daysnew = dir(path)
	daysnew = daysnew[grepl("csv",daysnew)]
	if(length(daysnew) == length(days))
	{
	  if(reprint == 1)
		{
			reprint = 0
			print('Waiting for new file dump')
		}
		Sys.sleep(10800)
		next
	}
	reprint = 1
	match = match(days,daysnew)
	days = daysnew
	daysnew = daysnew[-match]
	for(x in 1 : length(daysnew))
	{
	  print(paste('Processing',daysnew[x]))
		stitchFile(path,daysnew[x],pathwrite,ERRATAFIX)
		print(paste('Done',daysnew[x]))
	}
	daysnew1 = daysnew[grepl('Com',daysnew)]
	daysnew2 = daysnew[grepl('Gsi',daysnew)]
	daysnew3 = daysnew[grepl('SCL',daysnew)]
	daysnew4 = daysnew[grepl('SCS',daysnew)]
	daysnew5 = daysnew[grepl('Tem',daysnew)]
	
	if(length(daysnew1) < 1)
		daysnew1 = lastrecordeddate[1]
	if(length(daysnew2) < 1)
		daysnew2 = lastrecordeddate[2]
	if(length(daysnew3) < 1)
		daysnew3 = lastrecordeddate[3]
	if(length(daysnew4) < 1)
		daysnew4 = lastrecordeddate[4]
	if(length(daysnew5) < 1)
		daysnew5 = lastrecordeddate[5]

	write(c(as.character(daysnew1[length(daysnew1)]),as.character(daysnew2[length(daysnew2)]),
	as.character(daysnew3[length(daysnew3)]),as.character(daysnew4[length(daysnew4)]),
	as.character(daysnew5[length(daysnew5)])),pathdatelastread)
	lastrecordeddate[1] = as.character(daysnew1[length(daysnew1)])
	lastrecordeddate[2] = as.character(daysnew2[length(daysnew2)])
	lastrecordeddate[3] = as.character(daysnew3[length(daysnew3)])
	lastrecordeddate[4] = as.character(daysnew4[length(daysnew4)])
	lastrecordeddate[5] = as.character(daysnew5[length(daysnew5)])
gc()
ERRATAFIX=1 #for now once GSI reading is fine change this to 1
}
print('Out of loop')
sink()
