errHandle = file('/home/admin/Logs/LogsKH008SMail.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)
system('rm -R "/home/admin/Dropbox/Second Gen/[KH-008S]"')
source('/home/admin/CODE/KH008SDigest/runHistoryKH008.R')
require('mailR')
print('History done')
source('/home/admin/CODE/KH008SDigest/3rdGenDataKH008.R')
print('3rd gen data called')
source('/home/admin/CODE/MasterMail/timestamp.R')
source('/home/admin/CODE/Send_mail/sendmail.R')

source('/home/admin/CODE/KH008SDigest/aggregateInfo.R')
timetomins = function(x)
{
  hr = unlist(strsplit(x,":"))
  min = as.numeric(hr[2])
  hr = as.numeric(hr[1])
  return((hr * 60 + min + 1))
}
rf = function(x)
{
  return(format(round(as.numeric(x),2),nsmall=2))
}
rf1 = function(x)
{
  return(format(round(as.numeric(x),1),nsmall=1))
}
rf3 = function(x)
{
  return(format(round(as.numeric(x),3),nsmall=3))
}
checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}
bodyCustGlob = ""
date = Sys.Date()-1
year = substr(date,0,4)
yrmon = substr(date,0,7)

path = "/home/admin/Dropbox/SerisData/1min/[728]"
filename = paste("[728] ",date,".txt",sep="")
pathRead = paste(path,year,yrmon,filename,sep="/")
dataread = read.table(pathRead, sep = "\t", header = T)
waterlvl = round(mean(dataread[,21]),2)
preparebody = function(path)
{
  print("Enter Body Func")
  body = ""
  body = paste(body,"Site Name: CMIC",sep="")
  body = paste(body,"\n\nLocation: Touk Meas, Cambodia",sep="")
  body = paste(body,"\n\nO&M Code: KH-008",sep="")
  body = paste(body,"\n\nSystem Size: 9833.55 kWp",sep="")
  body = paste(body,"\n\nNumber of Energy Meters: 5",sep="")
  body = paste(body,"\n\nModule Brand / Model / Nos: Jinko Solar / JKM365M / 7768 / JKM325PP / 21533",sep="")
  body = paste(body,"\n\nInverter Brand / Model / Nos: SMA / Solid-Q50 / 154",sep="")
  body = paste(body,"\n\nSite COD: 2019-04-08",sep="")
  body = paste(body,"\n\nSystem age [days]: ",as.character((as.numeric(DAYSACTIVE))),sep="")
  body = paste(body,"\n\nSystem age [years]: ",as.character(round((as.numeric(DAYSACTIVE))/365,2)),sep="")
  bodyCust = body
  
  for(x in 1 : length(path))
  {
    print(path[x])
    body = paste(body,"\n\n_____________________________________________\n")
    bodyCust = paste(bodyCust,"\n\n_____________________________________________\n")
    days = unlist(strsplit(path[x],"/"))
    days = substr(days[length(days)],11,20)
    body = paste(body,days)
    bodyCust = paste(bodyCust,days)
    body  = paste(body,"\n_____________________________________________\n")
    bodyCust  = paste(bodyCust,"\n_____________________________________________\n")
    dataread = read.table(path[x],header = T,sep="\t",stringsAsFactors=F)
    print("___________________")
    print(path[x])
    print(ncol(dataread))
    body = paste(body,"\nPts Recorded: ",as.character(dataread[1,2])," (",round(as.numeric(dataread[1,2])/14.4,1),"%)",sep="")
    body = paste(body,"\n\nDaily Irradiation Pyranometer [kWh/m2]:",as.character(dataread[1,5]))
    body = paste(body,"\n\nDaily Irradiation GSi00 [kWh/m2]:",as.character(dataread[1,3]))
    bodyCust = paste(bodyCust,"\nDaily Irradiation GSi00 [kWh/m2]:",as.character(dataread[1,3]))
    body = paste(body,"\n\nDaily Irradiation Gmod [kWh/m2]:",as.character(dataread[1,4]))
    body = paste(body,"\n\nLast recorded time:",as.character(dataread[1,56]))
    body  = paste(body,"\n\n_______________________\n")
    body  = paste(body,"\n\tFULL SITE\n")
    body  = paste(body,"\n_______________________\n")
    
    body  = paste(body,paste("\nEac-1 [kWh]: ",as.character(dataread[1,76]),sep=""))
    bodyCust  = paste(bodyCust,paste("\n\nFull site energy generated [kWh]: ",as.character(dataread[1,77]),sep=""))
    
    body  = paste(body,paste("\n\nEac-2 [kWh]: ",as.character(dataread[1,77]),sep=""))
    body  = paste(body,paste("\n\nYield-1 [kWh/kWp]: ",as.character(dataread[1,78]),sep=""))
    body  = paste(body,paste("\n\nYield-2 [kWh/kWp]: ",as.character(dataread[1,79]),sep=""))
    body  = paste(body,paste("\n\nPR-1 [%]: ",as.character(dataread[1,80]),sep=""))
    body  = paste(body,paste("\n\nPR-2 [%]: ",as.character(dataread[1,81]),sep=""))
    
    GA_Pond = as.numeric(dataread[1,82])
    GA_R06 = as.numeric(dataread[1,83])
    GA_R11 = as.numeric(dataread[1,84])
    GA_R03 = as.numeric(dataread[1,85])
    GA_R02 = as.numeric(dataread[1,86])
    Grid_Availability = (GA_Pond*2835.32 + GA_R06*495.95 + GA_R11*1397.5 + GA_R03*2854.8 + GA_R02*2249.98)/(2835.32+495.95+1397.5+2854.8+2249.98)
    Grid_Availability = round(Grid_Availability,2)
    body  = paste(body,paste("\n\nGrid Availability [%]: ",as.character(Grid_Availability),sep=""))
    PA_Pond = as.numeric(dataread[1,87])
    PA_R06 = as.numeric(dataread[1,88])
    PA_R11 = as.numeric(dataread[1,89])
    PA_R03 = as.numeric(dataread[1,90])
    PA_R02 = as.numeric(dataread[1,91])
    Plant_Availability = (PA_Pond*2835.32 + PA_R06*495.95 + PA_R11*1397.5 + PA_R03*2854.8 + PA_R02*2249.98)/(2835.32+495.95+1397.5+2854.8+2249.98)
    Plant_Availability = round(Plant_Availability,2)
    body  = paste(body,paste("\n\nPlant Availability [%]: ",as.character(Grid_Availability),sep=""))
    
    bodyCust  = paste(bodyCust,paste("\n\nFull site performance ratio [%]: ",as.character(dataread[1,81]),sep=""))
    
    body  = paste(body,"\n\n_______________________\n")
    body  = paste(body,"\n\tPond\n")
    body  = paste(body,"\n_______________________\n")
    body  = paste(body,paste("\nEac-1 [kWh]: ",as.character(dataread[1,36]),sep=""))
    body  = paste(body,paste("\n\nEac-2 [kWh]: ",as.character(dataread[1,37]),sep=""))
    bodyCust  = paste(bodyCust,paste("\n\nPond energy generated [kWh]: ",as.character(dataread[1,37]),sep=""))
    #bodyCust  = paste(bodyCust,paste("\n\nPond energy generated [kWh]: 0"))
    body  = paste(body,paste("\n\nYield-1 [kWh/kWp]: ",as.character(dataread[1,38]),sep=""))
    body  = paste(body,paste("\n\nYield-2 [kWh/kWp]: ",as.character(dataread[1,39]),sep=""))
    body  = paste(body,paste("\n\nPR-1 [%]: ",as.character(dataread[1,40]),sep=""))
    body  = paste(body,paste("\n\nPR-2 [%]: ",as.character(dataread[1,41]),sep=""))
    #############
    body  = paste(body,paste("\n\nGrid Availability [%]: ",as.character(dataread[1,82]),sep=""))
    body  = paste(body,paste("\n\nPlant Availability [%]: ",as.character(dataread[1,87]),sep=""))
    #############
    body  = paste(body,paste("\n\nLast recorded reading [kW]: ",as.character(dataread[1,42]),sep=""))
    body  = paste(body,paste("\n\nLast recorded time[kW]: ",as.character(dataread[1,43]),sep=""))
    
    
    body  = paste(body,"\n\n_______________________\n")
    body  = paste(body,"\n\tR02\n")
    body  = paste(body,"\n_______________________\n")
    body  = paste(body,paste("\nEac-1 [kWh]: ",as.character(dataread[1,44]),sep=""))
    body  = paste(body,paste("\n\nEac-2 [kWh]: ",as.character(dataread[1,45]),sep=""))
    bodyCust  = paste(bodyCust,paste("\n\nR02 energy generated [kWh]: ",as.character(dataread[1,45]),sep=""))
    body  = paste(body,paste("\n\nYield-1 [kWh/kWp]: ",as.character(dataread[1,46]),sep=""))
    body  = paste(body,paste("\n\nYield-2 [kWh/kWp]: ",as.character(dataread[1,47]),sep=""))
    body  = paste(body,paste("\n\nPR-1 [%]: ",as.character(dataread[1,48]),sep=""))
    body  = paste(body,paste("\n\nPR-2 [%]: ",as.character(dataread[1,49]),sep=""))
    #############
    body  = paste(body,paste("\n\nGrid Availability [%]: ",as.character(dataread[1,86]),sep=""))
    body  = paste(body,paste("\n\nPlant Availability [%]: ",as.character(dataread[1,91]),sep=""))
    #############
    body  = paste(body,paste("\n\nLast recorded reading [kW]: ",as.character(dataread[1,50]),sep=""))
    body  = paste(body,paste("\n\nLast recorded time[kW]: ",as.character(dataread[1,51]),sep=""))
    
    
    body  = paste(body,"\n\n_______________________\n")
    body  = paste(body,"\n\tR03\n")
    body  = paste(body,"\n_______________________\n")
    body  = paste(body,paste("\nEac-1 [kWh]: ",as.character(dataread[1,52]),sep=""))
    body  = paste(body,paste("\n\nEac-2 [kWh]: ",as.character(dataread[1,53]),sep=""))
    bodyCust  = paste(bodyCust,paste("\n\nR03 energy generated [kWh]: ",as.character(dataread[1,53]),sep=""))
    body  = paste(body,paste("\n\nYield-1 [kWh/kWp]: ",as.character(dataread[1,54]),sep=""))
    body  = paste(body,paste("\n\nYield-2 [kWh/kWp]: ",as.character(dataread[1,55]),sep=""))
    body  = paste(body,paste("\n\nPR-1 [%]: ",as.character(dataread[1,56]),sep=""))
    body  = paste(body,paste("\n\nPR-2 [%]: ",as.character(dataread[1,57]),sep=""))
    #############
    body  = paste(body,paste("\n\nGrid Availability [%]: ",as.character(dataread[1,85]),sep=""))
    body  = paste(body,paste("\n\nPlant Availability [%]: ",as.character(dataread[1,90]),sep=""))
    #############
    body  = paste(body,paste("\n\nLast recorded reading [kW]: ",as.character(dataread[1,58]),sep=""))
    body  = paste(body,paste("\n\nLast recorded time[kW]: ",as.character(dataread[1,59]),sep=""))
    
    body  = paste(body,"\n\n_______________________\n")
    body  = paste(body,"\n\tR06\n")
    body  = paste(body,"\n_______________________\n")
    body  = paste(body,paste("\nEac-1 [kWh]: ",as.character(dataread[1,60]),sep=""))
    body  = paste(body,paste("\n\nEac-2 [kWh]: ",as.character(dataread[1,61]),sep=""))
    bodyCust  = paste(bodyCust,paste("\n\nR06 energy generated [kWh]: ",as.character(dataread[1,61]),sep=""))
    body  = paste(body,paste("\n\nYield-1 [kWh/kWp]: ",as.character(dataread[1,62]),sep=""))
    body  = paste(body,paste("\n\nYield-2 [kWh/kWp]: ",as.character(dataread[1,63]),sep=""))
    body  = paste(body,paste("\n\nPR-1 [%]: ",as.character(dataread[1,64]),sep=""))
    body  = paste(body,paste("\n\nPR-2 [%]: ",as.character(dataread[1,65]),sep=""))
    #############
    body  = paste(body,paste("\n\nGrid Availability [%]: ",as.character(dataread[1,83]),sep=""))
    body  = paste(body,paste("\n\nPlant Availability [%]: ",as.character(dataread[1,88]),sep=""))
    #############
    body  = paste(body,paste("\n\nLast recorded reading [kW]: ",as.character(dataread[1,66]),sep=""))
    body  = paste(body,paste("\n\nLast recorded time[kW]: ",as.character(dataread[1,67]),sep=""))
    
    
    body  = paste(body,"\n\n_______________________\n")
    body  = paste(body,"\n\tR11\n")
    body  = paste(body,"\n_______________________\n")
    body  = paste(body,paste("\nEac-1 [kWh]: ",as.character(dataread[1,68]),sep=""))
    body  = paste(body,paste("\n\nEac-2 [kWh]: ",as.character(dataread[1,69]),sep=""))
    bodyCust  = paste(bodyCust,paste("\n\nR11 energy generated [kWh]: ",as.character(dataread[1,69]),sep=""))
    body  = paste(body,paste("\n\nYield-1 [kWh/kWp]: ",as.character(dataread[1,70]),sep=""))
    body  = paste(body,paste("\n\nYield-2 [kWh/kWp]: ",as.character(dataread[1,71]),sep=""))
    body  = paste(body,paste("\n\nPR-1 [%]: ",as.character(dataread[1,72]),sep=""))
    body  = paste(body,paste("\n\nPR-2 [%]: ",as.character(dataread[1,73]),sep=""))
    #############
    body  = paste(body,paste("\n\nGrid Availability [%]: ",as.character(dataread[1,84]),sep=""))
    body  = paste(body,paste("\n\nPlant Availability [%]: ",as.character(dataread[1,89]),sep=""))
    #############
    body  = paste(body,paste("\n\nLast recorded reading [kW]: ",as.character(dataread[1,74]),sep=""))
    body  = paste(body,paste("\n\nLast recorded time[kW]: ",as.character(dataread[1,75]),sep=""))
    body  = paste(body,"\n\n_______________________")
    body = paste(body,"\n\nAverage Water Level:",waterlvl)
    body = paste(body,"\n\nMean Ambient Temp [C]:",as.character(dataread[1,6]))
    body = paste(body,"\n\nMean Ambient Temp Solar Hours [C]:",as.character(dataread[1,7]))
    body = paste(body,"\n\nMax Ambient Temp [C]:",as.character(dataread[1,8]))
    body = paste(body,"\n\nMax Ambient Temp Solar Hours [C]:",as.character(dataread[1,10]))
    body = paste(body,"\n\nMin Ambient Temp [C]:",as.character(dataread[1,9]))
    body = paste(body,"\n\nMin Ambient Temp Solar Hours [C]:",as.character(dataread[1,11]))
    body = paste(body,"\n\nMean Tmod [C]:",as.character(dataread[1,18]))
    body = paste(body,"\n\nMean Tmod Solar Hours [C]:",as.character(dataread[1,23]))
    body = paste(body,"\n\nMax Tmod [C]:",as.character(dataread[1,24]))
    body = paste(body,"\n\nMax Tmod Solar Hours [C]:",as.character(dataread[1,25]))
    body = paste(body,"\n\nMin Tmod [C]:",as.character(dataread[1,26]))
    body = paste(body,"\n\nMin Tmod Solar Hours [C]:",as.character(dataread[1,25]))
    body = paste(body,"\n\nDifference in mean Tmod and Tamb for Solar Hours [C]:",format((as.numeric(dataread[1,23])-as.numeric(dataread[1,7])),nsmall=1))
    body = paste(body,"\n\nMean Humidity [%]:",as.character(dataread[1,12]))
    body = paste(body,"\n\nMean Humidity Solar Hours [%]:",as.character(dataread[1,13]))
    body = paste(body,"\n\nMax Humidity [%]:",as.character(dataread[1,14]))
    body = paste(body,"\n\nMin Humidity [%]:",as.character(dataread[1,15]))
    body = paste(body,"\n\nMax Humidity Solar Hours [%]:",as.character(dataread[1,16]))
    body = paste(body,"\n\nMin Humidity Solar Hours [%]:",as.character(dataread[1,17]))
    body = paste(body,"\n\nGMod/GSi Ratio:",as.character(dataread[1,19]))
    body = paste(body,"\n\nPyr/Si Ratio:",as.character(dataread[1,20]))
    body = paste(body,"\n\nAverage Wind Direction:",as.character(dataread[1,21]))
    body = paste(body,"\n\nAverage Wind Direction Solar Hours:",as.character(dataread[1,22]))
    body = paste(body,"\n\nStdev Wind Direction [m/s]:",as.character(dataread[1,33]))
    body = paste(body,"\n\nCOV Wind Direction [%]:",as.character(dataread[1,34]))
    body = paste(body,"\n\nAverage Wind Speed [m/s]:",as.character(dataread[1,28]))
    body = paste(body,"\n\nMax Wind Speed [m/s]:",as.character(dataread[1,29]))
    body = paste(body,"\n\nAverage Wind Speed Solar hours [m/s]:",as.character(dataread[1,30]))
    body = paste(body,"\n\nStdev Wind Speed [m/s]:",as.character(dataread[1,31]))
    body = paste(body,"\n\nCOV Wind Speed [%]:",as.character(dataread[1,32]))
    print(as.character(dataread[1,31]))
    print(as.character(dataread[1,32]))
    print(as.character(dataread[1,33]))
    print(as.character(dataread[1,34]))
    print("______________")
  }
  print('Daily Summary for Body Done')
  body = paste(body,"\n\n_____________________________________________\n")
  
  body = paste(body,"\nStation History\n")
  body = paste(body,"\n_____________________________________________\n\n")
  
  body = paste(body,"Station Birth Date:",dobstation)
  body = paste(body,"\n\n# Days station active:",DAYSACTIVE)
  body = paste(body,"\n\n# Years station active:",rf1(DAYSACTIVE/365))
  body = paste(body,"\n\nTotal irradiation from uncleaned Si sensor this month [kWh/m2]:",MONTHLYIRR1)
  body = paste(body,"\n\nTotal irradiation from cleaned Si sensor this month [kWh/m2]:",MONTHLYIRR2)
  body = paste(body,"\n\nTotal irradiation  measured by Pyranometer [kWh/m2]:",MONTHLYIRR3)
  body = paste(body,"\n\nAverage irradiation from uncleaned Si sensor this month [kWh/m2]:",MONTHAVG1)
  body = paste(body,"\n\nAverage irradiation from cleaned Si sensor this month [kWh/m2]:",MONTHAVG2)
  body = paste(body,"\n\nAverage irradiation measured by Pyranometer this month [kWh/m2]:",MONTHAVG3)
  body = paste(body,"\n\nForecasted irradiation total for uncleaned Si sensor this month [kWh/m2]:",FORECASTIRR1);
  body = paste(body,"\n\nForecasted irradiance total for cleaned Si sensor this month [kWh/m2]:",FORECASTIRR2);
  body = paste(body,"\n\nForecasted irradiance total for Pyranamoter this month [kWh/m2]:",FORECASTIRR3)
  body = paste(body,"\n\nIrradiation total last 30 days from cleaned Si sensor [kWh/m2]:",LAST30DAYSTOT)
  body = paste(body,"\n\nIrradiation total last 30 days from Pyranometer [kWh/m2]:",LAST30DAYSTOT2)
  body = paste(body,"\n\nIrradiation average last 30 days from cleaned Si sensor [kWh/m2]:",LAST30DAYSMEAN)
  body = paste(body,"\n\nIrradiance avg last 30 days from Pyranometer[kWh/m2]:",LAST30DAYSMEAN2)
  body = paste(body,"\n\nSoiling loss (%):",SOILINGDEC)
  body = paste(body,"\n\nSoiling loss per day (%)",SOILINGDECPD)
  body = paste(body,"\n\nPyr/Si loss (%):",SNPDEC)
  body = paste(body,"\n\nPyr/Si loss per day (%)",SNPDECPD)
  print('3G Data Done')	
  bodyCustGlob <<- bodyCust
  return(body)
}

path = "/home/admin/Dropbox/SerisData/1min/[728]"
pathwrite = "/home/admin/Dropbox/Second Gen/[KH-008S]"
pathwriteCust = "/home/admin/Dropbox/Customer/[KH-9008S]"

checkdir(pathwrite)
checkdir(pathwriteCust)

date = format(Sys.time(),tz = "Singapore")
years = dir(path)
idxgs = match("_gsdata_",years)
if(is.finite(idxgs))
  years = years[-idxgs]
prevpathyear = paste(path,years[length(years)],sep="/")
months = dir(prevpathyear)
prevsumname = paste("[KH-008S] ",substr(months[length(months)],3,4),substr(months[length(months)],6,7),".txt",sep="")
prevpathmonth = paste(prevpathyear,months[length(months)],sep="/")
currday = prevday = dir(prevpathmonth)[length(dir(prevpathmonth))]
prevwriteyears = paste(pathwrite,years[length(years)],sep="/")
prevwritemonths = paste(prevwriteyears,months[length(months)],sep="/")
#if(length(dir(pathmonth)) > 1)
#{
#  prevday = dir(pathmonth[length(dir(pathmonth))-1])
#}

stnnickName = "728"
stnnickName2 = "KH-008S"
#lastdatemail = lastMailDate(paste('/home/admin/Start/MasterMail/',stnnickName2,'_Mail.txt',sep=""))
lastdatemail = lastMailDate(paste('/home/admin/Start/MasterMail/',stnnickName2,'_Mail.txt',sep=""))

if(!is.null(lastdatemail))
{
  yr = substr(lastdatemail,1,4)
  monyr = substr(lastdatemail,1,7)
  prevpathyear = paste(path,yr,sep="/")
  prevsumname = paste("[",stnnickName2,"] ",substr(lastdatemail,3,4),substr(lastdatemail,6,7),".txt",sep="")
  prevpathmonth = paste(prevpathyear,monyr,sep="/")
  currday = prevday = paste("[",stnnickName,"] ",lastdatemail,".txt",sep="")
  prevwriteyears = paste(pathwrite,yr,sep="/")
  prevwritemonths = paste(prevwriteyears,monyr,sep="/")
}

check = 0
sender <- "operations@cleantechsolar.com" 
uname <- "shravan.karthik@cleantechsolar.com"
pwd = 'CTS&*(789'
recipients = c('operationsKH@cleantechsolar.com','om-it-digest@cleantechsolar.com','youbunheng@yahoo.com','visalmeas27@gmail.com','vuthymao0714321918@gmail.com','phann.thul168@gmail.com','om-interns@cleantechsolar.com')
recipients9008 = c('operationsKH@cleantechsolar.com','gordon.breen@chipmonginsee.com','sokuntheary.siek@chipmonginsee.com' ,'veasna.hun@chipmonginsee.com','graeme.bride@chipmonginsee.com','sao.touch@chipmonginsee.com', 'bisakkda.uk@chipmonginsee.com', 'phearun.nguon@chipmonginsee.com', 'samol.prum@chipmonginsee.com', 'rithea.chhan@chipmonginsee.com', 'Pich.ben@chipmonginsee.com', 'Sophea.pheach@chipmonginsee.com', 'kimchhun.try@chipmonginsee.com', 'sereyroth.so@chipmonginsee.com', 'apinont.charoensupyanant@chipmonginsee.com','operationsKH@cleantechsolar.com','om-it-digest@cleantechsolar.com','youbunheng@yahoo.com','visalmeas27@gmail.com','vuthymao0714321918@gmail.com','phann.thul168@gmail.com','om-interns@cleantechsolar.com')
wt =1
idxdiff = 1
while(1)
{
  
recipients = c('operationsKH@cleantechsolar.com','om-it-digest@cleantechsolar.com','youbunheng@yahoo.com','visalmeas27@gmail.com','vuthymao0714321918@gmail.com','phann.thul168@gmail.com','om-interns@cleantechsolar.com')
recipients9008 = c('operationsKH@cleantechsolar.com','gordon.breen@chipmonginsee.com','sokuntheary.siek@chipmonginsee.com','veasna.hun@chipmonginsee.com' ,'graeme.bride@chipmonginsee.com','sao.touch@chipmonginsee.com', 'bisakkda.uk@chipmonginsee.com', 'phearun.nguon@chipmonginsee.com', 'samol.prum@chipmonginsee.com', 'rithea.chhan@chipmonginsee.com', 'Pich.ben@chipmonginsee.com', 'Sophea.pheach@chipmonginsee.com', 'kimchhun.try@chipmonginsee.com', 'sereyroth.so@chipmonginsee.com', 'apinont.charoensupyanant@chipmonginsee.com','operationsKH@cleantechsolar.com','om-it-digest@cleantechsolar.com','youbunheng@yahoo.com','visalmeas27@gmail.com','vuthymao0714321918@gmail.com','phann.thul168@gmail.com','om-interns@cleantechsolar.com')
  recordTimeMaster("KH-008S","Bot")
  years = dir(path)
  idxgs = match("_gsdata_",years)
  if(is.finite(idxgs))
    years = years[-idxgs]
  writeyears = paste(pathwrite,years[length(years)],sep="/")
  writeyearsCust = paste(pathwriteCust,years[length(years)],sep="/")
  
  checkdir(writeyears)
  checkdir(writeyearsCust)
  
  pathyear = paste(path,years[length(years)],sep="/")
  months = dir(pathyear)
  writemonths = paste(writeyears,months[length(months)],sep="/")
  sumname = paste("[KH-008S] ",substr(months[length(months)],3,4),substr(months[length(months)],6,7),".txt",sep="")
  sumnameCust = paste("[KH-9008S] ",substr(months[length(months)],3,4),substr(months[length(months)],6,7),".txt",sep="")
  checkdir(writemonths)
  pathmonth = paste(pathyear,months[length(months)],sep="/")
  if(length(dir(pathmonth)) < 1)
  {
    print('No file in the folder yet..')
    Sys.sleep(3600)
    next
  }
  tm = timetomins(substr(format(Sys.time(),tz = "Singapore"),12,16))
  currday = dir(pathmonth)[length(dir(pathmonth))]
  if(prevday == currday)
  {
    if(wt == 1)
    {
      print("")
      print("")
      print("__________________________________________________________")
      print(substr(currday,7,16))
      print("__________________________________________________________")
      print("")
      print("")
      print('Waiting')
      wt = 0
    }
    {
      if(tm > 1319 || tm < 30)
      {
        Sys.sleep(120)
      }        
      else
      {
        Sys.sleep(3600)
      }
    }
    next
  }
  print('New Data found.. sleeping to ensure sync complete')
  Sys.sleep(20)
  print('Sleep Done')
  idxdiff = match(currday,dir(pathmonth)) - match(prevday,dir(prevpathmonth))
  if(idxdiff < 0)
  {
    idxdiff = length(dir(prevpathmonth)) - match(prevday,dir(prevpathmonth)) + length(dir(pathmonth))
    newmonth = pathmonth
    newwritemonths = writemonths
    pathmonth = prevpathmonth
    writemonths = prevwritemonths
  }
  while(idxdiff)
  {
    {
      if(prevday == dir(prevpathmonth)[length(dir(prevpathmonth))])
      {
        pathmonth = newmonth
        writemonths = newwritemonths
        currday = dir(pathmonth)[1]
        monthlyirr1 = monthlyirr2 = monthlyirr3 = 0
        strng = unlist(strsplit(months[length(months)],"-"))
        daystruemonth = nodays(strng[1],strng[2])
        dayssofarmonth = 0
      }	
      else
        currday = dir(pathmonth)[(match(prevday,dir(pathmonth)) +1)]
    }
    dataread = read.table(paste(pathmonth,currday,sep='/'),header = T,sep = "\t",stringsAsFactors=F)
    datawrite = prepareSumm(dataread)
    
    datasum = rewriteSumm(datawrite)
    datasumCust = getCustData(datasum)
    
    currdayw = gsub("728","KH-008S",currday)
    write.table(datawrite,file = paste(writemonths,currdayw,sep="/"),append = F,sep="\t",row.names = F,col.names = T)
    
    
    
    monthlyirr1 = monthlyirr1 + as.numeric(datawrite[1,3])
    monthlyirr2 = monthlyirr2 + as.numeric(datawrite[1,4])
    monthlyirr3 = monthlyirr3 + as.numeric(datawrite[1,5])
    globirr1 = globirr1 + as.numeric(datawrite[1,3])
    globirr2 = globirr2 + as.numeric(datawrite[1,4])
    globirr3 = globirr3 + as.numeric(datawrite[1,5])
    dayssofarmonth = dayssofarmonth + 1    
    daysactive = daysactive + 1
    last30days[[last30daysidx]] = as.numeric(datawrite[1,4])
    last30days2[[last30daysidx]] = as.numeric(datawrite[1,5])
    
    
    
    print('Digest Written');
    {
      if(!file.exists(paste(writemonths,sumname,sep="/")))
      {
        write.table(datasum,file = paste(writemonths,sumname,sep="/"),append = F,sep="\t",row.names = F,col.names = T)
        print('Summary file created')
      }
      else 
      {
        write.table(datasum,file = paste(writemonths,sumname,sep="/"),append = T,sep="\t",row.names = F,col.names = F)
        print('Summary file updated')  
      }
    }
    {
      if(!file.exists(paste(writeyearCust,sumfilenameCust,sep="/")) || (x == 1 && y == 1 && z==1))
      {
        write.table(datasumCust,file = paste(writeyearCust,sumfilenameCust,sep="/"),append = F,sep="\t",row.names = F,col.names = T)
      }
      else 
      {
        write.table(datasumCust,file = paste(writeyearCust,sumfilenameCust,sep="/"),append = T,sep="\t",row.names = F,col.names = F)
      }
    }
    
    filetosendpath = c(paste(writemonths,currdayw,sep="/"))
    filename = c(currday)
    filedesc = c("Daily Digest")
    dataread = read.table(paste(prevpathmonth,prevday,sep='/'),header = T,sep = "\t",stringsAsFactors=F)
    datawrite = prepareSumm(dataread)
    prevdayw = gsub("728","KH-008S",prevday)
    
    dataprev = read.table(paste(prevwritemonths,prevdayw,sep="/"),header = T,sep = "\t",stringsAsFactors=F)
    print('Summary read')
    if(as.numeric(datawrite[,2]) != as.numeric(dataprev[,2]))
    {
      print('Difference in old file noted')
      
      
      
      previdx = (last30daysidx - 1) %% 31
      
      if(previdx == 0) {previdx = 1}
      datasum = rewriteSumm(datawrite)
      prevdayw = gsub("728","KH-008S",prevday)
      
      write.table(datawrite,file = paste(prevwritemonths,prevdayw,sep="/"),append = F,sep="\t",row.names = F,col.names = T)
      
      
      if(prevpathmonth == pathmonth)
      {
        monthlyirr1 = monthlyirr1 + as.numeric(datawrite[1,3]) - as.numeric(dataprev[1,3])
        monthlyirr2 = monthlyirr2 + as.numeric(datawrite[1,4]) - as.numeric(dataprev[1,4])
        monthlyirr3 = monthlyirr3 + as.numeric(datawrite[1,5]) - as.numeric(dataprev[1,5])
      }
      globirr1 = globirr1 + as.numeric(datawrite[1,3]) - as.numeric(dataprev[1,3])
      globirr2 = globirr2 + as.numeric(datawrite[1,4]) - as.numeric(dataprev[1,4])
      globirr3 = globirr3 + as.numeric(datawrite[1,5]) - as.numeric(dataprev[1,5])
      last30days[[previdx]] = as.numeric(datawrite[1,4])
      last30days2[[previdx]] = as.numeric(datawrite[1,5])
      
      
      
      datarw = read.table(paste(prevwritemonths,prevsumname,sep="/"),header = T,sep = "\t",stringsAsFactors=F)
      rn = match(as.character(datawrite[,1]),as.character(datarw[,1]))
      print(paste('Match found at row no',rn))
      datarw[rn,] = datasum[1,]
      write.table(datarw,file = paste(prevwritemonths,prevsumname,sep="/"),append = F,sep="\t",row.names = F,col.names = T)
      filetosendpath[2] = paste(prevwritemonths,prevdayw,sep="/")
      filename[2] = prevday
      filedesc[2] = c("Updated Digest")
      print('Updated old files.. both digest and summary file')
    }
    
    last30daysidx = (last30daysidx + 1 ) %% 31
    if(last30daysidx == 0) {last30daysidx = 1}
    
    LAST30DAYSTOT = rf(sum(last30days))
    LAST30DAYSMEAN = rf(mean(last30days))
    LAST30DAYSTOT2 = rf(sum(last30days2))
    LAST30DAYSMEAN2 = rf(mean(last30days2))
    DAYSACTIVE = daysactive
    MONTHLYIRR1 = monthlyirr1
    MONTHLYIRR2 = monthlyirr2
    MONTHLYIRR3 = monthlyirr3
    MONTHAVG1 = rf(monthlyirr1 / dayssofarmonth)
    MONTHAVG2 = rf(monthlyirr2 / dayssofarmonth)
    MONTHAVG3 = rf(monthlyirr3 / dayssofarmonth)
    FORECASTIRR1 = rf(as.numeric(MONTHAVG1) * daystruemonth)
    FORECASTIRR2 = rf(as.numeric(MONTHAVG2) * daystruemonth)
    FORECASTIRR3 = rf(as.numeric(MONTHAVG3) * daystruemonth)
    SOILINGDEC = rf3(((globirr1 / globirr2) - 1) * 100)
    SNPDEC = rf3(((globirr3 / globirr2) - 1) * 100)
    SOILINGDECPD = rf3(as.numeric(SOILINGDEC) / DAYSACTIVE)
    SNPDECPD = rf3(as.numeric(SNPDEC) / DAYSACTIVE)
    
    print('Sending mail')
    body = ""
    body = gsub("\n ","\n",body)
    bodyCustGlob <<- ""
    currday1 = substr(currday, 0, 16)
    print(currday1)
    
    graph_command1 = paste("Rscript /home/admin/CODE/EmailGraphs/KH008/PR_plot1.R", substr(currday,7,16), sep=" ")
    system(graph_command1)
    
    graph_command2 = paste("Rscript /home/admin/CODE/KH008SDigest/PR_Graph.R",'KH-008',74.5 ,0.008 ,"2019-04-12",substr(currday,7,16),sep=" ")
    system(graph_command2)
    
    graph_command3 = paste("python3 /home/admin/CODE/KH008SDigest/GHI_VS_PR.py",substr(currday,7,16),sep=" ")
    system(graph_command3)
    
    graph_path1=paste('/home/admin/Jason/cec intern/results/KH008S/[KH-008S] Graph - All Meters ',substr(currday,7,16),'.pdf',sep="")
    summary_txt=paste('/home/admin/Jason/cec intern/results/KH008S/Summary.txt',sep="")

    filestosendpath=character()
    graphs=c('Full Site','R06','R02','R03','R11','Pond')
    
    for(g in 1 : 6){
      graph_path=paste("/home/admin/Graphs/Graph_Output", 'KH-008', paste("[", 'KH-008', "] Graph",' - ',graphs[g],' - ', substr(currday,7,16), " - PR Evolution.pdf", sep=""), sep="/")
      filestosendpath=c(filestosendpath,graph_path)
    }

    #Graph Extract Paths
    for(g in 1 : 6){
    extract_path=paste("/home/admin/Graphs/Graph_Extract", 'KH-008', paste("[", 'KH-008', "] Graph",' - ',graphs[g],' - ', substr(currday,7,16), " - PR Evolution.txt", sep=""), sep="/")
    filestosendpath=c(filestosendpath,extract_path)
      }
    
    ghi_pr_graph_path=paste("/home/admin/Graphs/Graph_Output", 'KH-008', paste("[", 'KH-008', "] Graph",' - ', substr(currday,7,16), " - GHI vs PR.pdf", sep=""), sep="/")
    ghi_pr_extract_path=paste("/home/admin/Graphs/Graph_Extract", 'KH-008', paste("[", 'KH-008', "] Graph",' - ', substr(currday,7,16), " - GHI vs PR.txt", sep=""), sep="/")
    filestosendpath=c(ghi_pr_graph_path,ghi_pr_extract_path,filestosendpath)

    
    body = preparebody(filetosendpath)
    filetosendpath = c(filestosendpath, filetosendpath)
    print("Printing attachments")
    print(filetosendpath)
    
    send.mail(from = sender,
              to = c('operations@cleantechsolar.com','operationsKH@cleantechsolar.com'),
              #to = c('anusha.agarwal@cleantechsolar.com'),
              bcc = recipients,
              #to = recipients,
              subject = paste("Station [KH-008S] Digest",substr(currday,7,16)),
              body = body,
              smtp = list(host.name = "smtp.office365.com", port = 587, user.name = uname, passwd = pwd, tls = TRUE),
              authenticate = TRUE,
              send = TRUE,
              attach.files = filetosendpath,
              debug = F)

    filename9008 = paste("[KH-9008S] ",substr(currday,9,10),substr(currday,12,13),".txt",sep="")
    file9008 = paste("/home/admin/Dropbox/Customer/[KH-9008S]/",substr(currday,7,10),"/",filename9008,sep="")
    
    send.mail(from = sender,
              to = recipients9008,
              bcc = c('holger.eick@cleantechsolar.com'),
              
              subject = paste("Client [KH-9008S] Digest",substr(currday,7,16)),
              body = bodyCustGlob,
              smtp = list(host.name = "smtp.office365.com", port = 587, user.name = uname, passwd = pwd, tls = TRUE),
              authenticate = TRUE,
              send = TRUE,
              attach.files = file9008,
              file.names = filename9008, # optional parameter
              debug = F)
    print('Mail Sent')
    recordTimeMaster("KH-008S","Mail",substr(currday,7,16))
    prevday = currday
    prevpathyear = pathyear
    prevpathmonth = pathmonth
    prevwriteyears = writeyears
    prevwritemonths = writemonths
    prevsumname = sumname
    idxdiff = idxdiff - 1;
    wt = 1
  }
  gc()
}
sink()
