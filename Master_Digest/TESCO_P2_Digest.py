import requests, json
import requests.auth
import pandas as pd
import datetime
import os
import re 
import time
import shutil
import pytz
import sys
import smtplib
import numpy as np
from email.mime.text import MIMEText
from email.MIMEMultipart import MIMEMultipart
from email.MIMEBase import MIMEBase
from email import Encoders
import math
import logging
start = "2021-02-28"
tz = pytz.timezone('Asia/Calcutta')

recipients = ['operationsTH@cleantechsolar.com','om-it-digest@cleantechsolar.com']
sites = ['TH-030L','TH-031L','TH-032L','TH-033L','TH-034L','TH-035L','TH-036L','TH-037L','TH-038L','TH-039L','TH-040L','TH-041L','TH-042L','TH-043L','TH-044L']
sites_mfm = ['MFM_1_PV Meter','MFM_1_PV Meter','MFM_1_PV Meter','MFM_1_PV Meter','MFM_1_PV Meter','MFM_1_PV Meter','MFM_1_PV Meter','MFM_1_PV MFM','MFM_1_PV Meter','MFM_1_PV Meter','MFM_1_PV Meter','MFM_1_PV Meter','MFM_1_PV Meter','MFM_1_PV Meter','MFM_1_PV Meter']
sites_wms = ['WMS_5_Pyranometer','WMS_5_Pyranometer','WMS_5_Pyranometer','WMS_5_Pyranometer','WMS_5_Pyranometer','WMS_5_Pyranometer','WMS_5_Pyranometer','WMS_5_pyranometer','WMS_3_Pyranometer','WMS_5_Pyranometer','WMS_5_Pyranometer','WMS_1_Pyranometer','WMS_5_Pyranometer','WMS_5_Pyranometer','WMS_5_Pyranometer']
sites_cap = [998.6,999.6,999.6,999.6,489.6,489.6,782,517.2,326.4,326.4,448.8,564.4,517.2,809.2,782]
sites_name = ['Phase 2 - Bo Win','Phase 2 - Bang Yai','Phase 2 - Chaeng Watthana','Phase 2 - Pathum Thani','Phase 2 - Pak Thong Chai','Phase 2 - Chok Chai','Phase 2 - Det Udom','Phase 2 - Uthumphon Phisai','Phase 2 - Non Thai','Phase 2 - Prathai','Phase 2 - Chiang Yuen','Phase 2 - Mukdahan','Phase 2 - That Phanom','Phase 2 - Sawang Daen Din','Phase 2 - Tha Bo']

startpath = '/home/admin/Start/MasterMail/'
if(os.path.exists(startpath + 'TH-0P2' + "_Mail.txt")):
    print('Exists')
else:
    with open(startpath + 'TH-0P2' + "_Mail.txt", "w") as file:
        timenow = (datetime.datetime.now(tz)).strftime('%Y-%m-%d %H:%M:%S')
        file.write(timenow + "\n" + start)
    
with open(startpath + 'TH-0P2' + "_Mail.txt") as f:
    startdate = f.readlines()[1].strip()
print(startdate)
startdate = datetime.datetime.strptime(startdate, "%Y-%m-%d")

def send_mail(date, info, recipients, attachment_path_list=None):
    server = smtplib.SMTP("smtp.office365.com")
    server.starttls()
    server.login('shravan.karthik@cleantechsolar.com', 'CTS&*(789')
    msg = MIMEMultipart()
    sender = 'operations@cleantechsolar.com'
    msg['Subject'] = "TESCO PHASE-2 MASTER DIGEST " + str(date)
    #if sender is not None:
    msg['From'] = sender
    msg['To'] = ", ".join(recipients)
    if attachment_path_list is not None:
        for each_file_path in attachment_path_list:
            try:
                file_name = each_file_path.split("/")[-1]
                part = MIMEBase('application', "octet-stream")
                part.set_payload(open(each_file_path, "rb").read())
                Encoders.encode_base64(part)
                part.add_header('Content-Disposition', 'attachment' ,filename=file_name)
                msg.attach(part)
            except:
                print("could not attache file")
    msg.attach(MIMEText(info,'plain/text'))
    server.sendmail(sender, recipients, msg.as_string())


def get_data(site,mfm,wms,date):

    path = '/home/admin/Dropbox/Second Gen/[' + site + ']/' + str(date[:4]) + '/' + str(date[:7]) + '/'
    mfm_path = path + mfm + '/[' + site +']-'+ mfm[:3] + mfm[4] + '-'+ str(date) + '.txt'
    wms_path = path + wms + '/[' + site +']-'+ wms[:3] + wms[4] + '-'+ str(date) + '.txt'
    data = []
    df = pd.read_csv(mfm_path,sep='\t')
    data.append(df['Eac2'][0])
    data.append(df['Yld2'][0])
    data.append(df['PR2'][0])
    df = pd.read_csv(wms_path,sep='\t')
    data.append(df['GHI'][0])
    return data

def get_site(site,site_cap,site_name,data):
    body = "______________________________________________\n\n" + str(site) + " - " + site_name + "\n______________________________________________\n\n"
    body += "Installed Capacity [kWp]: " + str(site_cap) + "\n\n"
    body += "Energy Generated [kWh]: " + str(data[0])+ "\n\n"
    body += "Yield [kWh/kWp]: " + str(data[1]) + "\n\n"
    body += "Performance Ratio [%]: " +str(data[2]) + "\n\n"
    body += "Irradiation [kWh/m2]: " +str(data[3]) + "\n\n"
    return body

def get_info(sites,sites_cap,tgen,ylds,irrs,prs,ngen=0):
    body = "TESCO (PHASE - 2) - MASTER DIGEST \n______________________________________________\n\n"
    body += "Total Installed Capacity [kWp]: " + str(sum(sites_cap)) + "\n\n"
    body += "Total Sites Running: " + str(len(sites)) + "\n\n"
    body += "Number of Sites with Irradiance Sensor: " + str(len(sites)) + '\n\n'
    body += "Total Energy Generated [kWh]: " + str(round(tgen,1)) + '\n\n'
    body += "Total Yield [kWh/kWp]: " + str(round(tgen/sum(sites_cap),2)) + "\n\n"
    std=np.std(ylds)
    cov=(std*100/np.mean(ylds))
    body += "COV Yields [%]: " + str(round(cov,1)) +"\n\n"
    body += "Average Irradiation at sites with sensors [kWh/m2]: " + str(round(np.mean(irrs),1)) + '\n\n'
    std=np.std(irrs)
    cov=(std*100/np.mean(irrs))
    body += "COV Irradiation [%]: " + str(round(cov,1)) + "\n\n"
    body += "Estimated Portfolio PR [%]: " + str(round((tgen*100/sum(sites_cap))/np.mean(irrs),1)) + '\n\n'
    body += "Number of Sites with PR > 80 %: " + str(sum(1 for i in prs if i > 80)) + '\n\n'
    body += "Number of Sites with 70 < PR < 80 %: " + str(sum(1 for i in prs if i <= 80 and i > 70)) + '\n\n'
    body += "Number of Sites with PR < 70 %: " + str(sum(1 for i in prs if i <= 70)) + '\n\n'
    body += "Number of Sites with No Generation: " + str(ngen) + '\n\n'
    return body


while(1):
  try:
    date = (datetime.datetime.now(tz)).strftime('%Y-%m-%d')
    timenow = (datetime.datetime.now(tz)).strftime('%Y-%m-%d %H:%M:%S')
    with open(startpath + 'TH-0P2' + "_Bot.txt", "w") as file:
      file.write(timenow)   
    ylds = []
    irrs = []
    prs = []
    gen = 0
    ngen = 0
    content = ""
    for i in range(len(sites)):
        data = get_data(sites[i],sites_mfm[i],sites_wms[i],date)
        if(math.isnan(data[0])==False):
            gen += data[0]
            ylds.append(data[1])
            irrs.append(data[3])
            prs.append(data[2])
        else:
            print(sites[i])
            prs.append('NA')
            print("NAN ENCOUNTERED")
        content += get_site(sites[i],sites_cap[i],sites_name[i],data)
    content = get_info(sites,sites_cap,gen,ylds,irrs,prs,ngen) + content
    print('Done Processing')
    if(datetime.datetime.now(tz).hour == 2 and (datetime.datetime.now(tz).date()-startdate.date()).days>1):
      print('Sending')
      date_yesterday = (datetime.datetime.now(tz)+datetime.timedelta(days=-1)).strftime('%Y-%m-%d')
      ylds = []
      irrs = []
      prs = []
      gen = 0
      ngen = 0
      content = ""
      for i in range(len(sites)):
        data = get_data(sites[i],sites_mfm[i],sites_wms[i],date_yesterday)
        if(math.isnan(data[0])==False):
            gen += data[0]
            ylds.append(data[1])
            if(math.isnan(data[3])==False):
                irrs.append(data[3])
            if(math.isnan(data[2])==False):
                prs.append(data[2])
            else:
                prs.append(0)
        else:
            print("NA")
            ngen = ngen + 1
        content += get_site(sites[i],sites_cap[i],sites_name[i],data)
      content = get_info(sites,sites_cap,gen,ylds,irrs,prs,ngen) + content
      send_mail(date_yesterday,content,recipients)
      with open(startpath + 'TH-0P2' + "_Mail.txt", "w") as file:
          file.write(timenow + "\n" + date_yesterday)   
      startdate= datetime.datetime.strptime(date_yesterday,'%Y-%m-%d')
    print('Sleeping')
  except:
    print('error')
    logging.exception('msg')
  time.sleep(1200)