import pytz
import os
from ftplib import FTP
import time
import pytz
import io
import urllib.request
import re 
import gzip
import shutil
import logging
from database_operations import *
from locus_api import *
from process_functions import *
import process_functions

#Go through SERIS metadata.
site = 'IN-015'
site_id = get_site_id(site)
irr_center = 'Self'
data_granularity = get_data_granularity(site)
locus_parameters = []
azure_locus_parameters = []
component_id = []
tz = pytz.timezone("Asia/Calcutta")
main_path = '/home/pranav/New_Architecture/RawData/'

df_cols = pd.read_csv("Seris_Cols.csv")
print(df_cols)
mfmcols = df_cols.loc[df_cols['ComponentType']=='MFM','CSName'].tolist()
wmscols = df_cols.loc[df_cols['ComponentType']=='WMS','CSName'].tolist()

components = {'WMS':['3-9','WMS','WMSIRR',wmscols],'LV':['12-58','LV Meter','OTH',mfmcols],'HV':['59-105','HV Meter','MFM',mfmcols,1814.4,'IN-015A','a0T280000063PbfEAE']}

for j in components:
    if(components[j][2]=='WMSIRR'):
        c_id = add_component(components[j][1], 'WMSIRR', site_id)
    elif(components[j][2]=='MFM'):
        c_id = add_component(components[j][1], 'MFM', site_id)
        add_mfm_info_auto(c_id, site_id, components[j][4], components[j][5], components[j][6])
    elif(components[j][2]=='OTH'):
        c_id = add_component(components[j][1], 'OTH', site_id)
    print(len(','.join(components[j][3])))
    add_serismetadata(c_id, site_id, j,  ','.join(components[j][3]),components[j][0])
    all_parameters = get_all_parameters()
    for i in components[j][3]:
        if(i in all_parameters['CSName'].tolist()):
            parameter_id = all_parameters.loc[all_parameters['CSName'] == i,'ParameterId'].values[0]
            add_cleantech_metadata(parameter_id, c_id, site_id)
        else:
            print('Parameter Skipped!')

if(irr_center=='Self'):
    irr_site_id = site_id
else:
    irr_site_id = get_site_id(irr_center)

add_irr_center(site_id,irr_site_id)
#Need WMS First for PR calculation so sorted in get_ceantec_components function
component_df = get_cleantech_components(site)
print(component_df)
cleantech_component_id = component_df['ComponentId'].tolist()
locus_metadata_df = get_locus_metadata(site)
all_parameter_df = get_all_parameters()


for index,row in component_df.iterrows():
    #if silicon or pyranometer in name then use this else
    c_name = row['ComponentName']
    processed_parameters_dict = {"WMSIRR":["DA","GHI","Avg Tmod"],"WMS":["DA","Avg Tmod"],"MFM":['DA','EAC method-1','EAC method-2','Yield-1','Yield-2','PR-1 (GHI)','PR-2 (GHI)','Full Site PR (GHI)','Last recorded value','Grid Availability','Plant Availability'],"INV":['Yield-2','Inverter Availability'],'OTH':[]}
    for i in processed_parameters_dict[row['ComponentType']]:
        parameter_id = all_parameter_df.loc[all_parameter_df['MPName']==i,'ParameterId'].values[0]
        try:
            add_cleantech_metadata(parameter_id, row['ComponentId'], site_id)
        except:
            pass

#Adding MFM, INV COV
mfm_component_df = component_df.loc[component_df['ComponentType']=='MFM',:]


if(len(mfm_component_df)>1):
    last_mfm = mfm_component_df.tail(1)
    parameter_id = all_parameter_df.loc[all_parameter_df['MPName']=='Stdev','ParameterId'].values[0]
    add_cleantech_metadata(parameter_id, last_mfm['ComponentId'].values[0], site_id)
    parameter_id = all_parameter_df.loc[all_parameter_df['MPName']=='COV','ParameterId'].values[0]
    add_cleantech_metadata(parameter_id, last_mfm['ComponentId'].values[0], site_id)

parameter_df = get_parameter(site)

#Getting Capacities
mfm_capacity_df = get_mfm_capacities(site) 


#Raw Parameters
raw_parameter_df = parameter_df.loc[parameter_df['ParameterType']=='Raw',:]
parameter_name_mapping = dict(zip(raw_parameter_df['MPName'],raw_parameter_df['CSName']))
parameter_name_mapping['ts']='Timestamp'
parameter_name_mapping['id']='ComponentId' #This is mapped to LocusId initially

#Processed Parameters
processed_parameter_df = parameter_df.loc[parameter_df['ParameterType']=='Processed',:]

#Creatin Table
create_table(site,raw_parameter_df['CSName'].unique().tolist()) 




