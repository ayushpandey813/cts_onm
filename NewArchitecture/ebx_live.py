import pytz
import time
from database_operations import *

def get_source_id(site):
    query = pd.read_sql_query('''SELECT [Station_Id] FROM [dbo].[Stations] WHERE [Station_Name]='''+'\''+str(site)+'\'', cnxn)
    df = pd.DataFrame(query, columns=['Station_Id'])
    return df['Station_Id'].values[0]
    
def get_source_mfm(site):
    query = pd.read_sql_query("SELECT [Meter_id] FROM [dbo].[Meters] WHERE [Station_Id]=(SELECT [Station_Id] FROM [dbo].[Stations] WHERE [Station_Name] ="+"'"+site+"')", cnxn)
    df = pd.DataFrame(query, columns=['Meter_id'])
    return df['Meter_id'].values

def get_dest_id(site):
    query = pd.read_sql_query('''SELECT [SiteId] FROM [Portfolio].[Site] WHERE [SiteCode]='''+'\''+str(site)+'\'', cnxn)
    df = pd.DataFrame(query, columns=['SiteId'])
    return df['SiteId'].values[0]

def get_dest_mfm(site):
    query = pd.read_sql_query("SELECT [ComponentId] FROM [Portfolio].[Component] WHERE [SiteId]=(SELECT [SiteId] FROM [Portfolio].[Site] WHERE SiteCode ="+"'"+site+"')" +" AND ComponentType='MFM'", cnxn)
    df = pd.DataFrame(query, columns=['ComponentId'])
    return df['ComponentId'].values  

#Upsert Function
def upsert(Date, Value, ParameterId, ComponentId, SiteId, BypassFlag):
  cursor = cnxn.cursor()
  with cursor.execute("UPDATE [Portfolio].[ProcessedData] SET [Value]=?, [ParameterId]=?, [ComponentId]=?, [SiteId]=?, [BypassFlag]=? where [Date]=? AND [ComponentId]=? AND [ParameterId]=? if @@ROWCOUNT = 0 INSERT into [Portfolio].[ProcessedData]([Date], [Value], [ParameterId], [ComponentId], [SiteId], [BypassFlag]) values(?, ?, ?, ?, ?, ?)", [Value, ParameterId, ComponentId, SiteId, BypassFlag, Date, ComponentId, ParameterId, Date, Value, ParameterId, ComponentId, SiteId, BypassFlag]):
    pass
  cnxn.commit()

def fast_upsert_processed(site,df):
    cols=df.columns.tolist()
    str_site=site.replace('-','')
    query_1='DECLARE @Date_'+str_site+' datetime,'+'@value_'+str_site+' float,'+'@parameterid_'+str_site+' int,'
    query_vals='SET @Date_'+str_site+' =?\n'+'SET @value_'+str_site+ '=?\n'+'SET @parameterid_'+str_site+ '=?\n'
    query_2='UPDATE [Portfolio].[ProcessedData] SET [Date]=@Date_'+str_site+',[Value]=@value_'+str_site+','+'[ParameterId]=@parameterid_'+str_site+','
    query_3_1='INSERT INTO [Portfolio].[ProcessedData]([Date],[Value],[ParameterId],'
    query_3_2='VALUES(@Date_'+str_site+',@value_'+str_site+','+'@parameterid_'+str_site+','
    for i in cols[3:]:
        i = i.replace(' ','_')
        try:
            if(math.isnan(i)):
                continue
        except:
            pass
        query_vals=query_vals+'SET @'+i.replace('-','_')+'_'+str_site+'=?\n'
        if(i=='SiteId'):
            query_1=query_1+'@'+i.replace('-','_')+'_'+str_site+' int,'
        else:
            query_1=query_1+'@'+i.replace('-','_')+'_'+str_site+' float,'
        query_2=query_2+'['+i.replace('-','_')+']=@'+i.replace('-','_')+'_'+str_site+','
        query_3_1=query_3_1+'['+i.replace('-','_')+'],'
        query_3_2=query_3_2+'@'+i.replace('-','_')+'_'+str_site+','
    query_3=query_3_1[:-1]+')'+query_3_2[:-1]+')'
    final_query=query_1[:-1]+'\n'+query_vals[:-1]+'\n'+query_2[:-1]+' WHERE [Date] = @Date_'+str_site+' AND [ComponentId]=@Componentid_'+str_site+' AND [ParameterId]=@parameterid_'+str_site+'\nif @@ROWCOUNT = 0\n'+query_3
    cursor = cnxn.cursor()
    cursor.fast_executemany = True
    cursor.executemany(final_query, df.values.tolist())
    cnxn.commit()

cnxn = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
seris_site_df = get_sites('Seris',cnxn)
ebx_site_df = get_sites('Ebx',cnxn)
webdyn_site_df = get_sites('Webdyn',cnxn)
avisolar_site_df = get_sites('Avisolar',cnxn)
resync_site_df = get_sites('Resync',cnxn)
site_df = ebx_site_df.append(seris_site_df).append(webdyn_site_df).append(avisolar_site_df).append(resync_site_df)
print(site_df)

""" for index,row in site_df.iterrows():
    site=row['SiteCode']
    sourcesiteid = get_source_id(site)
    sourcecomponentid = get_source_mfm(site)
    destinationsiteid = get_dest_id(site)
    destinationcomponentid = get_dest_mfm(site)
    print(site)
    #Eac, Yld, LastR Upsert
    sourcesiteid = get_source_id(site)
    sourcecomponentid = get_source_mfm(site)
    destinationsiteid = get_dest_id(site)
    destinationcomponentid = get_dest_mfm(site)
    source_meters = pd.read_sql_query("SELECT [Meter_Id], [Reference] FROM [dbo].[Meters] where [Station_Id]=" + str(sourcesiteid), cnxn)
    dest_meters = pd.read_sql_query("SELECT [ComponentId], [MeterReference] FROM [Portfolio].[Meter] where [SiteId]=" + str(destinationsiteid), cnxn)
    source_dest_merge = pd.merge(source_meters, dest_meters, how='left', left_on=['Reference'], right_on=['MeterReference'])
    meterid_mapping = dict(zip(source_dest_merge.Meter_Id, source_dest_merge.ComponentId))
    print("Source_Dest_MeterID_Mapping:", meterid_mapping)
    source_data = pd.read_sql_query("SELECT [Date], [Meter_Id], [Eac-MFM], [LastR-MFM], [Yield], [Master-Flag] FROM Stations_Data where Station_Id=" + str(sourcesiteid), cnxn)
    source_data.columns = ['Date', 'Meter_Id', 'EAC Method-2', 'Last Recorded Value', 'Yield-2', 'BypassFlag']
    source_data['Date'] = source_data['Date'].dt.date
    parameter_mapping = {'EAC Method-2':5, 'Last Recorded Value':10, 'Yield-2':7}

    final = pd.DataFrame()
    for i in meterid_mapping:
        df_temp = source_data.copy()
        for j in parameter_mapping:
            final2 = df_temp[df_temp['Meter_Id'] == i]
            final2 = final2[['Date','Meter_Id',j,'BypassFlag']]
            final2['Meter_Id'] = meterid_mapping[i]
            final2['ParameterId'] = parameter_mapping[j]
            final2['SiteId'] = destinationsiteid
            final2 = final2[['Date',j,'ParameterId','Meter_Id','SiteId', 'BypassFlag']]
            final2.columns = ['Date', 'Value', 'ParameterId', 'ComponentId', 'SiteId', 'BypassFlag']
            final = final.append(final2)
            fast_upsert_processed(site,final2) 
        print(final['ParameterId'].unique())
    print(final)
    #fast_upsert_processed(site,final) 
    print('Done')

    
    #PR Upsert
    full_site_meter = pd.read_sql_query("SELECT [ComponentId] FROM [Portfolio].[CleantechMetadata] where ParameterId=69 and SiteId=" + str(destinationsiteid), cnxn)
    dest_ComponentId = full_site_meter['ComponentId'].values[0]
    final_df = pd.DataFrame(columns = ['Date', 'Value', 'ParameterId', 'ComponentId', 'SiteId', 'BypassFlag'])
    source_data = pd.read_sql_query("SELECT Stations_Data.Date, Stations_Data.Meter_Id, Meters.Capacity, Stations_Data.PR FROM Stations_Data INNER JOIN Meters ON Stations_Data.Meter_Id=Meters.Meter_id where Stations_Data.Station_Id=" + str(sourcesiteid), cnxn)
    source_data['Date'] = source_data['Date'].dt.date
    dates = source_data['Date'].unique()
    for date in dates:
        temp = source_data[source_data['Date']==date]
        temp['Weight'] = temp['Capacity']*temp['PR']
        temp_full_pr = temp['Weight'].sum()/temp['Capacity'].sum()
        final_df = final_df.append({'Date' : date,'Value' : temp_full_pr, 'ParameterId' : 69,'ComponentId' : int(dest_ComponentId),'SiteId' : int(destinationsiteid),'BypassFlag' : 0},ignore_index = True)
    print(final_df)
    fast_upsert_processed(site, final_df)
    cursor = cnxn.cursor()
    cursor.execute("DELETE FROM [Portfolio].[ProcessedData] WHERE SiteId=" + str(destinationsiteid) + " and ParameterId=69 AND ComponentId!=" + str(dest_ComponentId))
    cnxn.commit()

    #GHI Upsert
    wms_id = get_main_irr_component(destinationsiteid,cnxn)
    dest_ComponentId = wms_id
    print(dest_ComponentId)
    final_df = pd.DataFrame(columns = ['Date', 'Value', 'ParameterId', 'ComponentId', 'SiteId', 'BypassFlag'])
    source_data = pd.read_sql_query("SELECT Stations_Data.Date, Stations_Data.Meter_Id, Meters.Capacity, Stations_Data.GHI FROM Stations_Data INNER JOIN Meters ON Stations_Data.Meter_Id=Meters.Meter_id where Stations_Data.Station_Id=" + str(sourcesiteid), cnxn)
    source_data['Date'] = source_data['Date'].dt.date
    final_df = source_data.drop_duplicates(subset=['Date'])
    final_df['ParameterId'] = 2
    final_df['SiteId'] = destinationsiteid
    final_df['BypassFlag'] = 0
    final_df['ComponentId'] = wms_id
    final_df = final_df[['Date','GHI','ParameterId','ComponentId','SiteId', 'BypassFlag']]
    final_df.columns = ['Date', 'Value', 'ParameterId','ComponentId', 'SiteId', 'BypassFlag']
    print(final_df)
    fast_upsert_processed(site, final_df)
    print('Done')


cnxn.close() """



while(1):
    date_last_14 = (datetime.datetime.now() - datetime.timedelta(days=14)).strftime('%Y-%m-%d')
    print(date_last_14)
    cnxn = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
    fast_upsert_processed
    for index,row in site_df.iterrows():
        try:
            site=row['SiteCode']
            print(site)
            #PR Upsert
            sourcesiteid = get_source_id(site)
            sourcecomponentid = get_source_mfm(site)
            destinationsiteid = get_dest_id(site)
            destinationcomponentid = get_dest_mfm(site)
            source_meters = pd.read_sql_query("SELECT [Meter_Id], [Reference] FROM [dbo].[Meters] where [Station_Id]=" + str(sourcesiteid), cnxn)
            dest_meters = pd.read_sql_query("SELECT [ComponentId], [MeterReference] FROM [Portfolio].[Meter] where [SiteId]=" + str(destinationsiteid), cnxn)
            source_dest_merge = pd.merge(source_meters, dest_meters, how='left', left_on=['Reference'], right_on=['MeterReference'])
            meterid_mapping = dict(zip(source_dest_merge.Meter_Id, source_dest_merge.ComponentId))
            print("Source_Dest_MeterID_Mapping:", meterid_mapping)
            source_data = pd.read_sql_query("SELECT [Date], [Meter_Id], [Eac-MFM], [LastR-MFM], [Yield], [Master-Flag] FROM Stations_Data where Station_Id=" + str(sourcesiteid)+" AND Date>='"+date_last_14+"'", cnxn)
            source_data.columns = ['Date', 'Meter_Id', 'EAC Method-2', 'Last Recorded Value', 'Yield-2', 'BypassFlag']
            source_data['Date'] = source_data['Date'].dt.date
            parameter_mapping = {'EAC Method-2':5, 'Last Recorded Value':10, 'Yield-2':7}
            final = pd.DataFrame()
            for i in meterid_mapping:
                df_temp = source_data.copy()
                for j in parameter_mapping:
                    final2 = df_temp[df_temp['Meter_Id'] == i]
                    final2 = final2[['Date','Meter_Id',j,'BypassFlag']]
                    final2['Meter_Id'] = meterid_mapping[i]
                    final2['ParameterId'] = parameter_mapping[j]
                    final2['SiteId'] = destinationsiteid
                    final2 = final2[['Date',j,'ParameterId','Meter_Id','SiteId', 'BypassFlag']]
                    final2.columns = ['Date', 'Value', 'ParameterId', 'ComponentId', 'SiteId', 'BypassFlag']
                    final = final.append(final2)
            fast_upsert_processed(site,final) 

            #PR Upsert
            full_site_meter = pd.read_sql_query("SELECT [ComponentId] FROM [Portfolio].[CleantechMetadata] where ParameterId=69 and SiteId=" + str(destinationsiteid), cnxn)
            dest_ComponentId = full_site_meter['ComponentId'].values[0]
            final_df = pd.DataFrame(columns = ['Date', 'Value', 'ParameterId', 'ComponentId', 'SiteId', 'BypassFlag'])
            source_data = pd.read_sql_query("SELECT Stations_Data.Date, Stations_Data.Meter_Id, Meters.Capacity, Stations_Data.PR FROM Stations_Data INNER JOIN Meters ON Stations_Data.Meter_Id=Meters.Meter_id where Stations_Data.Station_Id=" + str(sourcesiteid)+" AND Date>='"+date_last_14+"'", cnxn)
            source_data['Date'] = source_data['Date'].dt.date
            dates = source_data['Date'].unique()
            for date in dates:
                temp = source_data[source_data['Date']==date]
                temp['Weight'] = temp['Capacity']*temp['PR']
                temp_full_pr = temp['Weight'].sum()/temp['Capacity'].sum()
                final_df = final_df.append({'Date' : date,'Value' : temp_full_pr, 'ParameterId' : 69,'ComponentId' : int(dest_ComponentId),'SiteId' : int(destinationsiteid),'BypassFlag' : 0},ignore_index = True)
            fast_upsert_processed(site, final_df)

            #GHI Upsert
            wms_id = get_main_irr_component(destinationsiteid,cnxn)
            dest_ComponentId = wms_id
            print(dest_ComponentId)
            final_df = pd.DataFrame(columns = ['Date', 'Value', 'ParameterId', 'ComponentId', 'SiteId', 'BypassFlag'])
            source_data = pd.read_sql_query("SELECT Stations_Data.Date, Stations_Data.Meter_Id, Meters.Capacity, Stations_Data.GHI FROM Stations_Data INNER JOIN Meters ON Stations_Data.Meter_Id=Meters.Meter_id where Stations_Data.Station_Id=" + str(sourcesiteid)+" AND Date>='"+date_last_14+"'", cnxn)
            source_data['Date'] = source_data['Date'].dt.date
            final_df = source_data.drop_duplicates(subset=['Date'])
            final_df['ParameterId'] = 2
            final_df['SiteId'] = destinationsiteid
            final_df['BypassFlag'] = 0
            final_df['ComponentId'] = wms_id
            final_df = final_df[['Date','GHI','ParameterId','ComponentId','SiteId', 'BypassFlag']]
            final_df.columns = ['Date', 'Value', 'ParameterId','ComponentId', 'SiteId', 'BypassFlag']
            fast_upsert_processed(site, final_df)
            print('Done')
        except:
            print('Failed')
    
    cnxn.close()
    print('Sleeping!')
    time.sleep(3600)


