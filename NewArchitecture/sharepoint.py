import pandas as pd
import sharepy
import datetime
import pytz
import pyodbc
import os
from calendar import monthrange
import math
import json
from database_operations import *
import numpy as np

def chkdir(path):
  if(os.path.isdir(path) == False):
    os.makedirs(path)

def upload_file(file_name,site,c_type,c_name,auth,sp_type):
    if(sp_type == 'Daily'):
        doc_library2="Data/Daily Values/"+site+"/"+c_type
        link = base_path + site_name + "_api/web/GetFolderByServerRelativeUrl('" + doc_library2 +"')/Files/add(url='"+c_name+'.csv'+"', overwrite=true)"

    else:
        doc_library2="Data/Monthly Values/"
        link = base_path + site_name + "_api/web/GetFolderByServerRelativeUrl('" + doc_library2 +"')/Files/add(url='"+c_name+'-LT.csv'+"', overwrite=true)"
    with open(file_name, 'rb') as read_file:
        content = read_file.read()
    status = auth.post(link, data=content)

def sharepoint_upload(path,site,c_type,c_name,auth):
    create_folder(site+'/'+c_type+'/',auth)
    upload_file(path,site,c_type,c_name,auth,'Daily')

def create_folder(path,auth):
    temp_path='Data/Daily Values/'
    for i in path.split('/')[:-1]:
        update_data = {}
        update_data['__metadata'] = {'type': 'SP.Folder'}
        temp_path=temp_path+i+'/'
        update_data['ServerRelativeUrl'] = temp_path
        body = json.dumps(update_data)
        link = base_path + site_name + "_api/web/folders/"
        status = auth.post(link,data=body) 
     

username = 'test-account@cleantechsolar.com'
password = 'Cleantechsolarpv@123'
site_name = "/OM/"
base_path = 'https://cleantechenergycorp.sharepoint.com'
doc_libary = "Data"


#For monthly
#Month  GHI PR Eac GA PA DA SA 

path='/home/admin/public/Daily Values/'
path_monthly='/home/admin/public/Monthly Values/'
chkdir(path)
chkdir(path_monthly)

sites = get_sites('Locus')

sites = sites['SiteCode'].tolist()
auth = sharepy.connect(base_path, username = username, password = password)
for site in sites:
    print(site)
    if(site=='MY-009'):
        capacities_df = get_mfm_capacities(site)
        parameter_df = get_all_parameters()
        component_df = get_cleantech_components(site)
        parameter_name_mapping = dict(zip(parameter_df['ParameterId'],parameter_df['CSName']))
        site_id = get_site_id(site)
        df = get_processed_data_fullsite(site_id)

        grouped_df = df.groupby('ComponentId')


        for c_id,group in grouped_df:
            component_name = component_df.loc[component_df['ComponentId']==c_id,'ComponentName'].values[0]
            component_type = component_df.loc[component_df['ComponentId']==c_id,'ComponentType'].values[0]
            df_temp = group.pivot( index = 'Date',columns='ParameterId', values=['Value'])
            df_temp.columns = df_temp.columns.get_level_values(1)
            df_temp.index.name = 'Date'
            df_temp.columns = df_temp.columns.to_series().map(parameter_name_mapping)
            chkdir(path + '/' + site + '/' + component_type)	
            df_temp.to_csv(path + '/' + site + '/' + component_type + '/' + component_name + '.csv')
            sharepoint_upload(path + '/' + site + '/' + component_type + '/' + component_name + '.csv',site,component_type,component_name,auth)
        
        try:
            mfm_df = pd.DataFrame()    
            for c_id in capacities_df['ComponentId'].tolist():
                temp_mfm_df = df[df['ComponentId'] == c_id]
                mfm_df = mfm_df.append(temp_mfm_df)

            #Adding GA,PA,DA
            grouped_mfm_df = mfm_df.groupby('ComponentId')
            df_monthly = pd.DataFrame()
            for c_id,group in grouped_mfm_df:
                df_temp = group.pivot( index = 'Date',columns='ParameterId', values=['Value'])
                df_temp.columns = df_temp.columns.get_level_values(1)
                df_temp.index.name = 'Date'
                df_temp.columns = df_temp.columns.to_series().map(parameter_name_mapping)
                if('Grid Availability' not in df_temp.columns.tolist()):
                    df_temp['Grid Availability'] = np.nan
                if('Plant Availability' not in df_temp.columns.tolist()):   
                    df_temp['Plant Availability'] = np.nan
                df_temp['System Availability'] = (df_temp['Grid Availability'] * df_temp['Plant Availability'])/100
                if(df_monthly.empty):
                    df_monthly = df_temp[['DA','Yield-2','Grid Availability','Plant Availability','System Availability']]*capacities_df.loc[capacities_df['ComponentId']==c_id,'Size'].values[0]
                    df_monthly_eac = df_temp['EAC method-2']
                else:
                    df_monthly = df_monthly+df_temp[['DA','Yield-2','Grid Availability','Plant Availability','System Availability']]*capacities_df.loc[capacities_df['ComponentId']==c_id,'Size'].values[0]
                    df_monthly_eac = df_monthly_eac + df_temp['EAC method-2']
            df_monthly = df_monthly/capacities_df['Size'].sum()
            df_monthly.rename(columns = {"DA": "MFM_DA"}, inplace = True)
            #Adding EAC
            df_monthly['EAC method-2'] = df_monthly_eac
            
            #Adding GHI/GTI
            irr_comp_id = get_main_irr_component(site_id)
            print(irr_comp_id)
            wms_df = get_processed_data_component(irr_comp_id)
            wms_df = wms_df[(wms_df['ParameterId'] == 2) | (wms_df['ParameterId'] == 304) | (wms_df['ParameterId'] == 1)]
            wms_df =wms_df.pivot( index = 'Date',columns='ParameterId', values=['Value'])
            wms_df.columns = wms_df.columns.get_level_values(1)
            wms_df.index.name = 'Date'
            wms_df.columns = wms_df.columns.to_series().map(parameter_name_mapping)
            wms_df.rename(columns = {"DA": "WMS_DA"}, inplace = True)
            df_monthly = pd.concat([df_monthly,wms_df])
            
            #Resampling
            df_monthly.index = pd.to_datetime(df_monthly.index)
            df_monthly = df_monthly.resample('1m').agg({'WMS_DA':'mean','GHI':'sum','MFM_DA':'mean','EAC method-2':'sum','Yield-2':'mean','Grid Availability':'mean','Plant Availability':'mean','System Availability':'mean'})
            df_monthly = df_monthly.round(2)
            df_monthly.insert(0,'Date',df_monthly.index.to_series().apply(lambda x: datetime.datetime.strftime(x, '%b-%Y')))
            df_monthly.to_csv(path_monthly  + site + '-LT.csv',index=False)
            upload_file(path_monthly  + site + '-LT.csv',site,'FULL SITE',site,auth,'Monthly')
            print(df_monthly)
        except Exception as e:
            print(e)
            print('Full Site failed for',site)
    



 
    





