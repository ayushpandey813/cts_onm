require('RCurl')
require('utils')
require('R.utils')
source('/home/admin/CODE/MasterMail/timestamp.R')
FIREMAIL = 0
retryCnt = 0
serverdownmail = function()
{
	send.mail(from = 'operations@cleantechsolar.com',
	          to = c('operations@cleantechsolar.com'),
						subject = "PH-006X FTP server down for more than 30'",
						body = " ",
						smtp = list(host.name = "smtp.office365.com", port = 587, user.name = 'shravan.karthik@cleantechsolar.com', passwd = 'CTS&*(789', tls = TRUE),
						authenticate = TRUE,
						send = TRUE,																												
						debug = F)
}
serverupmail = function()
{
	send.mail(from = 'operations@cleantechsolar.com',
	          to = c('operations@cleantechsolar.com'),
						subject = "PH-006X FTP server up and running",
						body = " ",
						smtp = list(host.name = "smtp.office365.com", port = 587, user.name = 'shravan.karthik@cleantechsolar.com', passwd = 'CTS&*(789', tls = TRUE),
						authenticate = TRUE,
						send = TRUE,																												
						debug = F)
}
dumpftp = function(days,path)
{
 	print('Enter function')
  url = "ftp://PH-006X:FJF7365RH73@ftpnew.cleantechsolar.com/"
  filenames = try(withTimeout(getURLContent(url,ftp.use.epsv = FALSE,dirlistonly = TRUE),
	 timeout = 600, onTimeout = "error"),silent=T)
	print('try success')
	if(class(filenames) == 'try-error')
	{
		retryCnt <<- retryCnt + 1
		print('Error in FTP server, will reconnect in 10 mins')
		if(FIREMAIL==0 && retryCnt > 3)
		{
			serverdownmail()
			FIREMAIL<<-1
		}
		return(0)
	}
	retryCnt <<- 0 
	if(FIREMAIL==1)
	{
	  print('Server up and running...')
		serverupmail()
		FIREMAIL<<-0
	}
	print('Filenames obtained')
  recordTimeMaster("PH-006X","FTPProbe")
	newfilenames = try(unlist(strsplit(filenames,"\n")),silent=T)
	if(class(newfilenames)=='try-error')
	{
		print('Error in strsplit')
		return(1)
	}
  newfilenames = newfilenames[grepl("zip",newfilenames)]
	tmps = newfilenames[grepl("tmp",newfilenames)]
	print(paste('newfilenames length before delete',length(newfilenames)))
	if(length(tmps) >= 1)
	{
		idxmtchrm = match(tmps,newfilenames)
		newfilenames = newfilenames[-idxmtchrm]
	}
	tmps = newfilenames[grepl("ComAp Intelimains _2_",newfilenames)]
	print(paste('newfilenames length before delete',length(newfilenames)))
	if(length(tmps) >= 1)
	{
		idxmtchrm = match(tmps,newfilenames)
		newfilenames = newfilenames[-idxmtchrm]
	}

	print('Zips found')
	newfilenamesPH006 = newfilenames[grepl("PH-006",newfilenames)]
	newfilenamesTmod = newfilenames[grepl("Tmod",newfilenames)]
	newfilenamesGsi00 = newfilenames[grepl("Gsi00",newfilenames)]
	newfilenamesComAp = newfilenames[grepl("ComAp Intelimains _2",newfilenames)]
	newfilenames = c(newfilenamesPH006,newfilenamesTmod,newfilenamesGsi00,newfilenamesComAp)
	days2 = try(unlist(strsplit(days,"\\.")),silent=T)
	if(class(days2)=='try-error')
	{
		print('error in strplit of days')
		return(1)
	}
	seq1 = seq(from = 1,to = length(days2),by=2)
	days2 = days2[seq1]
	print(paste('length of days2',length(days2)))
	newfilenames2 = try(unlist(strsplit(newfilenames,"\\.")),silent=T)
	if(class(days2)=='try-error')
	{
		print('Error in strsplit of newnames2')
		return(1)
	}
	seq1 = seq(from = 1,to = length(newfilenames2),by=2)
	newfilenames2 = newfilenames2[seq1]
	print(paste('length of newfilenames2',length(newfilenames2)))
	newfilenames3 = gsub("PH-006","PH-006X",newfilenames2)
	newfilenames3 = gsub("PH-006XX","PH-006X",newfilenames2)
	match = match(days2,newfilenames3)
	match = match[complete.cases(match)]
	if(length(match) < 1)
	{
	  print('No new files')
		return(1)
	}
  newfilenames = newfilenames[-match]
	if(length(newfilenames) < 1)
	{
	  print('No new files')
		return(1)
	}
	for(y in 1 : length(newfilenames))
  {
    urlnew = paste(url,newfilenames[y],sep="")
		namefile = gsub("zip","csv",newfilenames[y])
		if(grepl("PH-006",newfilenames[y]) && (!grepl("PH-006X",newfilenames[y])))
			namefile = gsub("PH-006","PH-006X",namefile)
		if(file.exists(paste(path,namefile,sep="/")))
		{
			print("file exists.. skip")
			next
		}
    file = try(download.file(urlnew,destfile = paste(path,newfilenames[y],sep="/")))
		if(class(file) == 'try-error')
		{
			print(paste('couldnt download',newfilenames[y]))
			next
		}
	  unzipping = try(unzip(paste(path,newfilenames[y],sep="/"),exdir = path),silent = T)
		{
		if(class(unzipping) == 'try-error')
		{
			print(paste('Couldnt extract',newfilenames[y]))
		}
		else
		{
			if(grepl("PH-006",newfilenames[y]) && (!grepl("PH-006X",newfilenames[y])))
			{
				update = gsub("zip","csv",newfilenames[y])
				updatenew = gsub("PH-006","PH-006X",update)
				file.rename(from=paste(path,update,sep="/"),to=paste(path,updatenew,sep="/"))
			}
		}
		}
		system(paste('rm "',path,'/',newfilenames[y],'"',sep = ""))
  }
	recordTimeMaster("PH-006X","FTPNewFiles",as.character(newfilenames[length(newfilenames)]))
	rm(filenames,newfilenames)
	gc()
	return(1)
}
