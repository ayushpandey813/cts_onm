rm(list = ls())
errHandle = file('/home/admin/Logs/LogsSERIS3G.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)

source("/home/admin/CODE/MasterMail/HQCoreFunction.R")

checkDir = function(path)
{
  if(!file.exists(path))
    dir.create(path)
}

create3G = function(dataread)
{
  path3G = "/home/admin/Dropbox/Third Gen/SERIS_SITES"
  colName = colnames(dataread)
  colExclude = c(1,2,3,4,5)
  colName = colName[-colExclude]
  type = as.character(dataread[,3])
  idxUse = which(type %in% "SERIS")
  dataread = dataread[idxUse,]
  stns = as.character(dataread[,1])
  print(length(stns))
  for(y in 1 : length(stns))
  {
    if(is.na(dataread[y,6]))
      next
    stnId = paste("[",stns[y],"]",sep="")
    pathNew = paste(path3G,stnId,sep="/")
    checkDir(pathNew)
    pathFinal = paste(pathNew,"/",stnId,"_lifetime.txt",sep="")
    if(!is.na(dataread[y,2]))
      pathFinal = paste(pathNew,"/",stnId,"-",as.character(dataread[y,2]),"_lifetime.txt",sep="")
    df = dataread[y,-colExclude]
    df = data.frame(df,stringsAsFactors = F)
    colnames(df) = colName
    appendOp = FALSE
    colOp = TRUE
    if(file.exists(pathFinal))
    {
      dfInt = read.table(pathFinal,header=T,sep="\t",stringsAsFactors = F)
      dateInt = as.character(dfInt[,1])
      print(length(dateInt))
      if(length(dateInt))
      {
        idxmtchInt = match(as.character(df[1,1]),dateInt)
        print(paste("Matching",as.character(df[1,1]),"and idxmtchInt is",idxmtchInt))
        if(is.finite(idxmtchInt))
        {
          dfInt = dfInt[-idxmtchInt,]
          write.table(dfInt,file=pathFinal,row.names = F, col.names = colOp,append=appendOp,sep="\t")
        }
      }
      appendOp = TRUE
      colOp = FALSE
    }
    write.table(df,file=pathFinal,row.names = F, col.names = colOp,append=appendOp,sep="\t")
  }
}

runHistory = function()
{
  system('rm -R "/home/admin/Dropbox/Third Gen/SERIS_SITES"')
  path3G = "/home/admin/Dropbox/Third Gen/SERIS_SITES"
  checkDir(path3G)
  startDate = as.Date("2016-03-01","%Y-%m-%d")
  endDate = Sys.Date()
  days = seq(startDate, endDate,by="day")
  for(x in 1:length(days))
  {
    dayPass = as.character(days[x])
    fileName = paste("/tmp/HQ_",dayPass,".txt",sep="")
    cmd = paste("rm '",fileName,"'",sep="")
    sendAggregateInfo(dayPass,"/tmp")
    dataread = read.table(fileName,header=T,sep="\t",stringsAsFactors = F)
    create3G(dataread)
    print(days[x])
    system(cmd)
  }
  write(as.character(endDate),"/home/admin/Start/SERIS_3G.txt")
}

RUNHISTORY=0
if(RUNHISTORY)
  runHistory()
pathLast = "/home/admin/Start/SERIS_3G.txt"

while(1)
{
  startDate = readLines(pathLast)
  startDate = as.Date(startDate[1],"%Y-%m-%d") - 2
  endDate = Sys.Date()
  dateSeq = seq.Date(startDate, endDate, by="day")
  for(x in 1 : length(dateSeq))
  {
    dayPass = as.character(dateSeq[x])
    fileName = paste("/tmp/HQ_",dayPass,".txt",sep="")
    cmd = paste("rm '",fileName,"'",sep="")
    sendAggregateInfo(dayPass,"/tmp")
    dataread = read.table(fileName,header=T,sep="\t",stringsAsFactors = F)
    create3G(dataread)
    system(cmd)
  }
  write(as.character(endDate),pathLast)
  Sys.sleep(3600*12) # Perform twice a day
}
sink()

