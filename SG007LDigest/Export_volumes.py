import datetime
import os
import pandas as pd
from matplotlib.pyplot import figure
import matplotlib.pyplot as plt
import matplotlib.colors as clrs
import matplotlib.ticker as ticker
from datetime import datetime
import matplotlib.dates as mdates
from datetime import datetime as dt

date = []
date_new = []
DA = []
DA_new = []
exp = []
path = '/home/admin/Dropbox/Third Gen/[SG-007E]/Final-Settlements/2020/'
fileList = os.listdir(path)
for file in sorted(fileList):
  file = os.path.join(path, file)
  with open(file) as f:
    df1 = pd.read_csv(file,sep='\t',header=0,skipinitialspace=True)
    df3 = pd.read_csv(file,sep='\t',header=1,skipinitialspace=True)
    df2 = df3.columns.tolist()
    date.append(df2[0])
    date1 = df1['Date']
    exp = df1['Export.Volume..kWh.'].sum()
    DA_new.append(exp)
for i in date:
   i = datetime.strptime(i, '%Y-%m-%d')
   i = i.strftime('%b-%Y')
   date_new.append(i)
last_date =  (df1['Date'].iloc[-1])

df = pd.DataFrame(list(zip(date_new, DA_new)),columns=['Date','DA_new'])
print(df)
#df.to_csv('/home/anusha/EMC/test3.csv')
#df.plot(x = "Date", y = "DA_new", kind = "bar", legend = False, stacked=True, colormap='Paired')
#fig.savefig('/home/anusha/TESCO/EMC_NEW_test.pdf')

fig = df.plot(x = "Date", y = "DA_new",kind='bar',legend = False, stacked=True, color='orange',  figsize=(13.2, 8.8), fontsize=11).get_figure()
fig.suptitle('SG-007E Export Volumes (Final Settlements)\n\n2020-01-01 to '+last_date, fontsize=15)
plt.ylabel('Export Volumn [kWh]', fontsize=11)
plt.xlabel(' ', fontsize=11)
plt.ylim(ymax=600)
fig.savefig('/home/admin/Graphs/SG-007L/SG-007L_export.pdf')
plt.show()
