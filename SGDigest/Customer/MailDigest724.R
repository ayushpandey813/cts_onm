rm(list = ls())
errHandle = file('/home/admin/Logs/LogsSG9005SMail.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)
system('rm -R "/home/admin/Dropbox/Customer/[SG-9005S]"')
APPENDWARNING = 0
source('/home/admin/CODE/SGDigest/Customer/runHistory724.R')
require('mailR')
print('History done')
source('/home/admin/CODE/SGDigest/Customer/3rdGenData724.R')
source('/home/admin/CODE/MasterMail/timestamp.R')
source('/home/admin/CODE/Send_mail/sendmail.R')
print('3rd gen data called')
full_site_energy = 0
timetomins = function(x)
{
  hr = unlist(strsplit(x,":"))
  min = as.numeric(hr[2])
  hr = as.numeric(hr[1])
  return((hr * 60 + min + 1))
}
rf = function(x)
{
  return(format(round(x,2),nsmall=2))
}
rf1 = function(x)
{
  return(format(round(x,1),nsmall=1))
}
rf3 = function(x)
{
  return(format(round(x,3),nsmall=3))
}
checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}

get725Gen1 = function(date)
{
  path = "/home/admin/Dropbox/SerisData/1min/[725]"
  yr = substr(date,7,10)
  mon = substr(date,7,13)
  dateAc = paste("[725] ",substr(date,7,16),".txt",sep="")
  pathret = paste(path,"/",yr,"/",mon,"/",dateAc,sep="")
  print("-------------------------")
 print(pathret)
  print("-------------------------")
  return(pathret)
}



subdata = subdata2 = data.frame()
preparebody = function(path)
{
  print("Enter Body Func")
  body=""
  body = paste(body,"Site Name: GlobalFoundries Woodlands\n",sep="")
  body = paste(body,"\nLocation: 60 Woodlands Industrial Park Street 2, Singapore 738406\n")
  body = paste(body,"\nSystem Size: 3,553.57 kWp\n")
  body = paste(body,"\nNumber of Energy Meters: 5\n")
  body = paste(body,"\nSystem sections:\n")
  body = paste(body,"\nFab-2: 794.97 kWp\n")
  body = paste(body,"\nFab-3/5: 1042.8 kWp\n")
  body = paste(body,"\nFab-7: 1229.8 kWp\n")
  body = paste(body,"\nFab-7G: 486 kWp\n")
  body = paste(body,"\nFab-2 start of operations: 2018-10-09\n")
  body = paste(body,"\nFab 3/5 start of operations: 2019-03-26\n")
  body = paste(body,"\nFab 7 start of operations: 2019-10-22\n")
  body = paste(body,"\nFab 7G start of operations: 2020-01-21\n")
  body = paste(body,"\nFab-2 System age [days]:",(daysactive-96),"\n")
  body = paste(body,"\nFab-2 System age [years]:",round((daysactive-96)/365,1))
  body = paste(body,"\n\nFab-3/5 System age [days]:",(daysactive-96-165),"\n")
  body = paste(body,"\nFab-3/5 System age [years]:",round((daysactive-96-165)/365,1))
  body = paste(body,"\n\nFab-7 System age [days]:",(daysactive-96-165-213),"\n")
  body = paste(body,"\nFab-7 System age [years]:",round((daysactive-96-165-213)/365,1))
  body = paste(body,"\n\nFab-7G System age [days]:",(daysactive-96-165-213-91),"\n")
  body = paste(body,"\nFab-7G System age [years]:",round((daysactive-96-165-213-91)/365,1))

  for(x in 1 : length(path))
  {
    print(path[x])
    dataread = read.table(path[x],header = T,sep="\t")
    if(x==1)
    {
      subdata <<- dataread[,c(1,2,4,9,21,15,31,33,37,40,41,38,39,43,45,47,48,49,50,51,53,55,57,58,59,61,63,65,66,67,69,71,73,74,75, 76, 77, 78, 79,80,81,82,83,84,85)]
      colnames(subdata) = c("Date","DA","Irradiation","Tamb","Tmod","Hamb","Energy","Yield","PR","LastReadSolSys","LastReadACDBLd","LastReadNetSol","LastTime",
                            "EnergyFab3/5","YieldFab3/5","PRFab3/5","LastTimeFab3/5","LastReadFab3/5","LastReadRec3/5","LastReadNet3/5",
                            "EnergyFab7/1","YieldFab7/1","PRFab7/1","LastTimeFab7/1","LastReadFab7/1",
                            "EnergyFab7/2","YieldFab7/2","PRFab7/2","LastTimeFab7/2","LastReadFab7/2",
                            "EnergyFab7Tot","YieldFab7Tot","PRFab7Tot","EnergyFab7G-1","EnergyFab7G-2", "YieldFab7G-1", "YieldFab7G-2", "PR1Fab7G","PR2Fab7G", "LastRecordedEnergy7G", "LastRecordedTime7G", "LastRecordedEnergyRecFab7G","LastRecordedTimeRec7G","ACDBLoads7G","NetSolar7G")
    }
    if(x==2)
    {
      subdata2 <<- dataread[,c(1,2,4,9,21,15,31,32,33,37,40,41,38,39,43,45,47,48,49,50,51,53,55,57,58,59,61,63,65,66,67,69,71,73,74,75,76,77,78,79,80,81,82,83,84,85)]
      colnames(subdata2) = c("Date","DA","Irradiation","Tamb","Tmod","Hamb","Energy","Yield","PR","LastReadSolSys","LastReadACDBLd","LastReadNetSol","LastTime",
                             "EnergyFab3/5","YieldFab3/5","PRFab3/5","LastTimeFab3/5","LastReadFab3/5","LastReadRec3/5","LastReadNet3/5",
                             "EnergyFab7/1","YieldFab7/1","PRFab7/1","LastTimeFab7/1","LastReadFab7/1",
                             "EnergyFab7/2","YieldFab7/2","PRFab7/2","LastTimeFab7/2","LastReadFab7/2",
                             "EnergyFab7Tot","YieldFab7Tot","PRFab7Tot","EnergyFab7G-1","EnergyFab7G-2", "YieldFab7G-1", "YieldFab7G-2", "PR1Fab7G","PR2Fab7G", "LastRecordedEnergy7G", "LastRecordedTime7G", "LastRecordedEnergyRecFab7G","LastRecordedTimeRec7G","ACDBLoads7G","NewSolar7G")
    }
    body = paste(body,"\n\n_____________________________________________\n")
    days = unlist(strsplit(path[x],"/"))
    days = substr(days[length(days)],12,21)
    full_site_energy = as.character(dataread[1,31]+dataread[1,43]+dataread[1,69]+dataread[1,76])
    body = paste(body,days,"Weather Parameters")
    body  = paste(body,"\n_____________________________________________\n\n")
    body = paste(body,"Pts Recorded: ",as.character(dataread[1,2])," (",round(as.numeric(dataread[1,2])/14.4,1),"%)",sep="")
    body = paste(body,"\n\nDaily Irradiation [kWh/m2]:",as.character(dataread[1,4]))
    GTI = dataread[1,4]
    body = paste(body,"\n\nMean Tamb (solar hours) [C]:",as.character(dataread[1,9]))
    body = paste(body,"\n\nMean Tmod (solar hours) [C]:",as.character(dataread[1,21]))
    body = paste(body,"\n\nMean Hamb (solar hours) [%]:",as.character(dataread[1,15]))
    body  = paste(body,"\n\n_____________________________________________\n")
    body = paste(body,"\n",days," FULL SITE\n",sep="")
    body = paste(body,"\n_____________________________________________\n")
    body = paste(body,"\nEnergy Generated FULL SITE [kWh]:",full_site_energy)
    body = paste(body,"\n\nYield FULL SITE [kWh/kWp]:",round((as.numeric(full_site_energy)/3553.57),2))
    body = paste(body,"\n\nPerformance Ratio FULL SITE [%]:",round(((as.numeric(full_site_energy)*100/3553.57)/as.numeric(dataread[1,4])),1))
    body = paste(body,"\n\n_____________________________________________\n")
    body = paste(body,"\n",days," Fab-2\n",sep="")
    body = paste(body,"_____________________________________________\n")
    body = paste(body,"\nEnergy Generated Fab-2 [kWh]:",as.character(dataread[1,31]))
    body = paste(body,"\n\nYield Fab-2 [kWh/kWp]:",as.character(dataread[1,33]))
    body = paste(body,"\n\nPerformance Ratio Fab-2 [%]:",as.character(dataread[1,37]))
    body = paste(body,"\n\nLast Recorded Active Energy Delivered (solar system) [kWh]:",as.character(dataread[1,40]))
    body = paste(body,"\n\nLast Recorded Active Energy Consumed (ACDB loads) [kWh]:",as.character(dataread[1,41]))
    body = paste(body,"\n\nLast Recorded Active Energy Delivered (net solar) [kWh]:",as.character(dataread[1,38]))
    body = paste(body,"\n\nLast recorded time:",as.character(dataread[1,39]))
    body  = paste(body,"\n\n_____________________________________________\n")
    body = paste(body,"\n",days," Fab-3/5\n",sep="")
    body  = paste(body,"_____________________________________________\n")
    body = paste(body,"\nEnergy Generated Fab-3/5 [kWh]:",as.character(dataread[1,43]))
    body = paste(body,"\n\nYield Fab-3/5 [kWh/kWp]:",as.character(dataread[1,45]))
    body = paste(body,"\n\nPerformance Ratio Fab-3/5 [%]:",as.character(format(round(dataread[1,47],1), nsmall = 1)))
    body = paste(body,"\n\nLast Recorded Active Energy Delivered (solar system) [kWh]:",as.character(dataread[1,49]))
    body = paste(body,"\n\nLast Recorded Active Energy Consumed (load) [kWh]:",as.character(dataread[1,50]))
    body = paste(body,"\n\nLast Recorded Active Energy Delivered (net solar) [kWh]:",as.character(dataread[1,51]))
    body = paste(body,"\n\nLast recorded time:",as.character(dataread[1,48]))
    body  = paste(body,"\n\n_____________________________________________\n")
    body = paste(body,"\n",days," Fab-7\n",sep="")
    body  = paste(body,"_____________________________________________\n")
    body = paste(body,"\nEnergy Generated Fab-7 [kWh]:",as.character(dataread[1,69]))
    body = paste(body,"\n\nYield Fab-7 [kWh/kWp]:",as.character(dataread[1,71]))
    body = paste(body,"\n\nPerformance Ratio Fab-7 [%]:",as.character(format(round(dataread[1,73],1), nsmall = 1)))
    body = paste(body,"\n\nLast Recorded Active Energy Delivered (Meter-1) [kWh]:",as.character(dataread[1,59]))
    body = paste(body,"\n\nLast Recorded Active Energy Consumed (ACDB loads) [kWh]:",as.character(format(round(dataread[74],2), nsmall = 2)))
    body = paste(body,"\n\nLast Recorded Active Energy Delivered (net solar) [kWh]:",format(round(as.numeric(dataread[1,59] - dataread[1,74]),2),nsmall=2))
    body = paste(body,"\n\nLast recorded time (Meter-1):",as.character(dataread[1,58]))
    body = paste(body,"\n\nLast Recorded Active Energy Delivered (Meter-2) [kWh]:",as.character(dataread[1,75]))
    body = paste(body,"\n\nLast Recorded Active Energy Consumed (ACDB loads) [kWh]:",as.character(format(round(dataread[1,67],2), nsmall = 2)))
    body = paste(body,"\n\nLast Recorded Active Energy Delivered (net solar) [kWh]:",format(round(as.numeric(dataread[1,75] - dataread[1,67]),2),nsmall=2))
    body = paste(body,"\n\nLast recorded time (Meter-2):",as.character(dataread[1,66]))
    #body = paste(body,"\n Stdev/COV Yields: ",format(round(as.numeric(dataread[,47])/as.numeric(dataread[,48]),2),nsmall=2),"/",as.character(dataread[,48]),"%",sep=" ")
    body  = paste(body,"\n\n_____________________________________________\n")
    body = paste(body,"\n",days," Fab-7G\n",sep="")
    body  = paste(body,"_____________________________________________\n")
    body = paste(body,"\nEnergy Generated Fab-7G [kWh]:",as.character(dataread[1,76]))
    body = paste(body,"\n\nYield Fab-7G [kWh/kWp]:",as.character(dataread[1,79]))
    Yield = dataread[1,79]
    PR = round((Yield / GTI),1)*100
    body = paste(body,"\n\nPerformance Ratio Fab-7G [%]:",PR)
    body = paste(body,"\n\nLast Recorded Active Energy Delivered (solar system) [kWh]:",as.character(dataread[1,83]))
    body = paste(body,"\n\nLast Recorded Active Energy Consumed (ACDB loads) [kWh]:",as.character(dataread[1,84]))
    body = paste(body,"\n\nLast Recorded Active Energy Delivered (net solar) [kWh]:",as.character(dataread[1,85]))
    body = paste(body,"\n\nLast recorded time:",as.character(dataread[1,82]))
    body  = paste(body,"\n\n_____________________________________________\n")
  }
  print('Daily Summary for Body Done')
  
  print('3G Data Done')	
  body = gsub("\n ","\n",body)
  return(body)
}
path = "/home/admin/Dropbox/SerisData/1min/[724]"
pathwrite = "/home/admin/Dropbox/Customer/[SG-9005S]"
path2 = "/home/admin/Dropbox/Second Gen/[SG-726S]"
checkdir(pathwrite)
date = format(Sys.time(),tz = "Singapore")
years = dir(path)
prevpathyear = paste(path,years[length(years)],sep="/")
months = dir(prevpathyear)
prevsumname = paste("[SG-9005S] ",substr(months[length(months)],3,4),substr(months[length(months)],6,7),".txt",sep="")
prevpathmonth = paste(prevpathyear,months[length(months)],sep="/")
currday = prevday = dir(prevpathmonth)[length(dir(prevpathmonth))]
prevwriteyears = paste(pathwrite,years[length(years)],sep="/")
prevwritemonths = paste(prevwriteyears,months[length(months)],sep="/")
#if(length(dir(pathmonth)) > 1)
#{
#  prevday = dir(pathmonth[length(dir(pathmonth))-1])
#}
stnnickName = "724"
stnnickName2 = "SG-9005S"
lastdatemail = lastMailDate(paste('/home/admin/Start/MasterMail/',stnnickName2,'_Mail.txt',sep=""))
if(!is.null(lastdatemail))
{
  yr = substr(lastdatemail,1,4)
  monyr = substr(lastdatemail,1,7)
  prevpathyear = paste(path,yr,sep="/")
  prevsumname = paste("[",stnnickName2,"] ",substr(lastdatemail,3,4),substr(lastdatemail,6,7),".txt",sep="")
  prevpathmonth = paste(prevpathyear,monyr,sep="/")
  currday = prevday = paste("[",stnnickName,"] ",lastdatemail,".txt",sep="")
  prevwriteyears = paste(pathwrite,yr,sep="/")
  prevwritemonths = paste(prevwriteyears,monyr,sep="/")
}

check = 0
sender <- "operations@cleantechsolar.com"
uname <- "shravan.karthik@cleantechsolar.com" 
pwd = 'CTS&*(789'
recipients <- c('operationsSG@cleantechsolar.com','natanasabapathy.v@globalfoundries.com','myaaye.maung@globalfoundries.com','abdullah.abdulhalim@globalfoundries.com','immanueledward.johnalwin@globalfoundries.com','om-interns@cleantechsolar.com','om-it-digest@cleantechsolar.com')
#"jeeva.govindarasu@cleantechsolar.com")

wt =1
idxdiff = 1
while(1)
{
  #recipients <- getRecipients("SG-999S","m")
  recipients <- c('operationsSG@cleantechsolar.com','natanasabapathy.v@globalfoundries.com','myaaye.maung@globalfoundries.com','abdullah.abdulhalim@globalfoundries.com','immanueledward.johnalwin@globalfoundries.com','om-interns@cleantechsolar.com','om-it-digest@cleantechsolar.com')
  recordTimeMaster("SG-9005S","Bot")
  years = dir(path)
  writeyears = paste(pathwrite,years[length(years)],sep="/")
  checkdir(writeyears)
  pathyear = paste(path,years[length(years)],sep="/")
  months = dir(pathyear)
  writemonths = paste(writeyears,months[length(months)],sep="/")
  sumname = paste("[SG-9005S] ",substr(months[length(months)],3,4),substr(months[length(months)],6,7),".txt",sep="")
  checkdir(writemonths)
  pathmonth = paste(pathyear,months[length(months)],sep="/")
  tm = timetomins(substr(format(Sys.time(),tz = "Singapore"),12,16))
  currday = dir(pathmonth)[length(dir(pathmonth))]
  if(prevday == currday)
  {
    if(wt == 1)
    {
      print("")
      print("")
      print("__________________________________________________________")
      print(substr(currday,7,16))
      print("__________________________________________________________")
      print("")
      print("")
      print('Waiting')
      wt = 0
    }
    {
      if(tm > 1319 || tm < 30)
      {
        Sys.sleep(120)
      }        
      else
      {
        Sys.sleep(3600)
      }
    }
    next
  }
  print('New Data found.. sleeping to ensure sync complete')
  Sys.sleep(0)
  print('Sleep Done')
  m1 = match(currday,dir(pathmonth)) 
  m2 = match(prevday,dir(prevpathmonth))
  idxdiff = m1 - m2
  if(!is.finite(idxdiff))
  {
    print('############## Error in idx ####################')
    print(paste('Currday',currday))
    print(paste('pathmonth',pathmonth))
    print(paste('prevday',prevday))
    print(paste('prevpathmonth',prevpathmonth))
    print(paste('m1',m1))
    print(paste('m2',m2))
    print('###############################################')
    next	
  }
  if(idxdiff < 0)
  {
    idxdiff = length(dir(prevpathmonth)) - match(prevday,dir(prevpathmonth)) + length(dir(pathmonth))
    newmonth = pathmonth
    newwritemonths = writemonths
    pathmonth = prevpathmonth
    writemonths = prevwritemonths
  }
  while(idxdiff)
  {
    {
      if(prevday == dir(prevpathmonth)[length(dir(prevpathmonth))])
      {
        pathmonth = newmonth
        writemonths = newwritemonths
        currday = dir(pathmonth)[1]
        monthlyirr1 = monthlyirr2 = monthlyirr3 = 0
        strng = unlist(strsplit(months[length(months)],"-"))
        daystruemonth = nodays(strng[1],strng[2])
        dayssofarmonth = 0
      }	
      else
        currday = dir(pathmonth)[(match(prevday,dir(pathmonth)) +1)]
    }
    fileat = c(paste(pathmonth,currday,sep="/"))
    if(file.exists(get725Gen1(currday)))
      fileat = c(fileat,get725Gen1(currday))
    dataread = read.table(paste(pathmonth,currday,sep='/'),header = T,sep = "\t")
    datawrite = prepareSumm(dataread,1)
    datasum = rewriteSumm(datawrite)
    currdayw = gsub("724","SG-9005S",currday)
    write.table(datawrite,file = paste(writemonths,currdayw,sep="/"),append = F,sep="\t",row.names = F,col.names = T)
    
    
    
    #monthlyirr1 = monthlyirr1 + as.numeric(datawrite[1,3])
    #monthlyirr2 = monthlyirr2 + as.numeric(datawrite[1,4])
    #monthlyirr3 = monthlyirr3 + as.numeric(datawrite[1,5])
    #globirr1 = globirr1 + as.numeric(datawrite[1,3])
    #globirr2 = globirr2 + as.numeric(datawrite[1,4])
    #globirr3 = globirr3 + as.numeric(datawrite[1,5])
    #dayssofarmonth = dayssofarmonth + 1    
    daysactive = daysactive + 1
    #last30days[[last30daysidx]] = as.numeric(datawrite[1,4])
    #last30days2[[last30daysidx]] = as.numeric(datawrite[1,5])
    
    
    
    print('Digest Written');
    {
      if(!file.exists(paste(writemonths,sumname,sep="/")))
      {
        write.table(datasum,file = paste(writemonths,sumname,sep="/"),append = F,sep="\t",row.names = F,col.names = T)
        
        print('Summary file created')
      }
      else 
      {
        write.table(datasum,file = paste(writemonths,sumname,sep="/"),append = T,sep="\t",row.names = F,col.names = F)
        dtint = read.table(paste(writemonths,sumname,sep="/"),header=T, sep="\t")
        dtint = dtint[(length(dtint[,1]) - match(unique(as.character(dtint[,1])),rev(as.character(dtint[,1])))+1),]
        
        write.table(dtint,file = paste(writemonths,sumname,sep="/"),append = F,sep="\t",row.names = F,col.names = T)
        print('Summary file updated')  
      }
    }
    filetosendpath = c(paste(writemonths,currdayw,sep="/"))
    filename = c(currday)
    filedesc = c("Daily Digest")
    dataread = read.table(paste(prevpathmonth,prevday,sep='/'),header = T,sep = "\t",stringsAsFactors=F)
    datawrite = prepareSumm(dataread,1)
    prevdayw = gsub("724","SG-9005S",prevday)
    dataprev = read.table(paste(prevwritemonths,prevdayw,sep="/"),header = T,sep = "\t",stringsAsFactors=F)
    print('Summary read')
    if(as.numeric(datawrite[,2]) != as.numeric(dataprev[,2]))
    {
      print('Difference in old file noted')
      fileat = c(fileat,paste(prevwritemonths,prevdayw,sep="/"))
      if(file.exists(get725Gen1(prevday)))
        fileat = c(fileat,get725Gen1(prevday))
      
      #previdx = (last30daysidx - 1) %% 31
      
      #if(previdx == 0) {previdx = 1}
      datasum = rewriteSumm(datawrite)
      write.table(datawrite,file = paste(prevwritemonths,prevdayw,sep="/"),append = F,sep="\t",row.names = F,col.names = T)
      
      
      #if(prevpathmonth == pathmonth)
      #{
      #	monthlyirr1 = monthlyirr1 + as.numeric(datawrite[1,3]) - as.numeric(dataprev[1,3])
      #  monthlyirr2 = monthlyirr2 + as.numeric(datawrite[1,4]) - as.numeric(dataprev[1,4])
      #	monthlyirr3 = monthlyirr3 + as.numeric(datawrite[1,5]) - as.numeric(dataprev[1,5])
      #}
      #globirr1 = globirr1 + as.numeric(datawrite[1,3]) - as.numeric(dataprev[1,3])
      #globirr2 = globirr2 + as.numeric(datawrite[1,4]) - as.numeric(dataprev[1,4])
      #globirr3 = globirr3 + as.numeric(datawrite[1,5]) - as.numeric(dataprev[1,5])
      #last30days[[previdx]] = as.numeric(datawrite[1,4])
      #last30days2[[previdx]] = as.numeric(datawrite[1,5])
      
      
      
      datarw = read.table(paste(prevwritemonths,prevsumname,sep="/"),header = T,sep = "\t",stringsAsFactors=F)
      rn = match(as.character(datawrite[,1]),as.character(datarw[,1]))
      print(paste('Match found at row no',rn))
      datarw[rn,] = datasum[1,]
      write.table(datarw,file = paste(prevwritemonths,prevsumname,sep="/"),append = F,sep="\t",row.names = F,col.names = T)
      filetosendpath[2] = paste(prevwritemonths,prevdayw,sep="/")
      filename[2] = prevday
      filedesc[2] = c("Updated Digest")
      print('Updated old files.. both digest and summary file')
    }
    
    print('Sending mail')
    body = ""
    body = preparebody(filetosendpath)
    write.table(subdata,file="/tmp/file1.txt",row.names=F,col.names=T,sep="\t",append=F)
    write.table(subdata2,file="/tmp/file2.txt",row.names=F,col.names=T,sep="\t",append=F)
    fileat = c(fileat,paste(writemonths,sumname,sep="/"))
    filename = c(paste("[724] ",subdata[1,1],".txt",sep=""))
    filename = c(filename,paste("[725] ",subdata[1,1],".txt",sep=""))
    if(length(fileat) > 4)
    {
      filename = c(filename,paste("[724] ",subdata2[1,1],".txt",sep=""))
      filename = c(filename,paste("[725] ",subdata2[1,1],".txt",sep=""))
    }
    filename= c(filename,paste("[724] ",substr(subdata[1,1],1,7),".txt",sep=""))
    subdata <<- data.frame()
    subdata2 <<- data.frame()
    print("##############")
    print(fileat)
    print(filename)
    print("##############")
    send.mail(from = sender,
              to = recipients,
              bcc = c('rajasekar@cutechgroup.com', 'arunsenthil@cutechgroup.com'),
              subject = paste("Client [SG-9005S] Digest",substr(currday,7,16)),
              body = body,
              smtp = list(host.name = "smtp.office365.com", port = 587, user.name = uname, passwd = pwd, tls = TRUE),
              authenticate = TRUE,
              send = TRUE,
              attach.files = fileat,
              file.names = filename, # optional parameter
              debug = F)
    print('Mail Sent');
    recordTimeMaster("SG-9005S","Mail",substr(currday,7,16))
    prevday = currday
    prevpathyear = pathyear
    prevpathmonth = pathmonth
    prevwriteyears = writeyears
    prevwritemonths = writemonths
    prevsumname = sumname
    idxdiff = idxdiff - 1;
    wt = 1
  }
  gc()
}
sink()
