import pandas as pd
import datetime
import sharepy
import io
import pyodbc

def chk_date(date):
    try:
        date=datetime.datetime.strptime(date, '%Y-%m-%d %H:%M:%S')
        return 1
    except:
        return 0


    
#Azure
server = 'cleantechsolar.database.windows.net'
database = 'Cleantech Meter Readings'
username = 'RohanKN'
password = 'R@h@nKN1'
driver= '{ODBC Driver 17 for SQL Server}'

connStr = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
cursor = connStr.cursor()
stations = pd.read_sql_query('''  SELECT  * FROM [dbo].[Stations] ''', connStr)
df_stations = pd.DataFrame(stations, columns=['Station_Name','Station_Id','Provider'])

#Sharepoint
username = 'test-account@cleantechsolar.com'
password = 'Cleantechsolarpv@123'
site_name = "/OM/"
base_path = 'https://cleantechenergycorp.sharepoint.com'
doc_libary = "Customer Invoicing/Meter Readings/AA Master Meter Readings"
files=['Meter Readings - India.xlsx','Meter Readings - Southeast Asia.xlsx']
s = sharepy.connect(base_path, username = username, password = password)
for i in files:
    #Pushing file to sharepoint
    link=base_path + site_name  + doc_libary + "/"+i
    r = s.get(link)

    #Save data to BytesIO Stream
    bytes_file_obj = io.BytesIO()
    bytes_file_obj.write(r.content)
    bytes_file_obj.seek(0) 

    #Read excel file and each sheet into pandas dataframe 
    df_dict = pd.read_excel(bytes_file_obj, sheet_name = None,engine='openpyxl')

    for index,row in df_stations.iterrows():
        try:
            df_site=df_dict[row['Station_Name'].strip()]
            temp_col=[i for i in range(len(df_site.columns.tolist())) ]
            df_site.columns=temp_col
            cols=df_site.loc[((df_site[0]=='#') & (df_site[1]=='Month of Invoicing')),:]
            idx=cols.index.values[0]
            df_site=df_site.iloc[idx+1:]
            df_site.columns=cols.values[0].tolist()
            df_site=df_site[['Month of Invoicing','3-M YIELD TREND','12-M YIELD TREND','LT YIELD TREND']]
            df_site['Month of Invoicing'] = pd.to_datetime(df_site['Month of Invoicing'], errors='coerce').dt.date
            df_site = df_site.dropna(subset=['Month of Invoicing'])
            df_site = df_site.dropna(subset=['3-M YIELD TREND'])
            df_site = df_site.where(pd.notnull(df_site), 'NULL')
            print(row)
            for index2,row2 in df_site.iterrows():
                cursor.execute('UPDATE [AllSites].[SystemPerformance] SET [3mYieldTrend] = '+str(row2['3-M YIELD TREND'])+',[12mYieldTrend] = '+str(row2['12-M YIELD TREND'])+',[LtYieldTrend] = '+str(row2['LT YIELD TREND'])+' WHERE  [Date] = \''+str(row2['Month of Invoicing'])+'\' AND [Station_Id] = '+str(row['Station_Id'])+'\nif @@ROWCOUNT = 0 \n   INSERT INTO [AllSites].[SystemPerformance]([Station_Id],[Date],[3mYieldTrend],[12mYieldTrend],[LtYieldTrend]) VALUES ('+str(row['Station_Id'])+',\''+str(row2['Month of Invoicing'])+'\','+str(row2['3-M YIELD TREND'])+','+str(row2['12-M YIELD TREND'])+','+str(row2['LT YIELD TREND'])+')')
                cursor.commit()
        except Exception as e:
            print('Failed')
connStr.close() 